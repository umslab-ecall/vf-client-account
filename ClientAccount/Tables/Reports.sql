﻿CREATE TABLE [dbo].[Reports]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
    [CategoryId] INT NOT NULL, 
    [Name] NVARCHAR(150) NOT NULL, 
    [Description] NVARCHAR(250) NULL, 
    [URLReports] NVARCHAR(MAX) NULL, 
    [SQLCommand] NVARCHAR(MAX) NULL, 
    [StoredProc] NVARCHAR(250) NULL, 
    [Param] NVARCHAR(MAX) NULL,
    [Order] INT NULL, 
    [PictureUrl] NVARCHAR(250) NULL, 
    [Active] BIT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [UpdatedOn] DATETIME NULL, 
    [UpdatedBy] NVARCHAR(50) NULL 
)
