﻿CREATE TABLE [dbo].[Promo]
(
	[CampaignID] INT NOT NULL , 
    [StartDateTime] DATETIME NOT NULL, 
    [EndDateTime] DATETIME NOT NULL, 
    [CountCalls] INT NOT NULL, 
    [TimeHours] NVARCHAR(20) NULL
)
