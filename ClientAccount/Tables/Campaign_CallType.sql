﻿CREATE TABLE [dbo].[Campaign_CallType]
(
	[CampaignID] INT NOT NULL , 
    [CampaignTypeId] INT NOT NULL, 
    [CallTypeID] INT NOT NULL, 
    [EnterpriseName] NVARCHAR(32) NOT NULL
)
