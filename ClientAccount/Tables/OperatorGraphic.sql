﻿CREATE TABLE [dbo].[OperatorGraphic]
(
	[CampaignID] INT NOT NULL , 
    [StartDateTime] DATETIME NOT NULL, 
    [EndDateTime] DATETIME NOT NULL, 
    [OperatorCount] INT NOT NULL, 
    [TimeHours] NVARCHAR(20) NULL
)
