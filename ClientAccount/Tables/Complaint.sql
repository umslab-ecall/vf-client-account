﻿CREATE TABLE [dbo].[Complaint]
(
	[ComplaintId] INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
    [ComplaintDescription] NVARCHAR(250) NOT NULL, 
    [CreatedBy] NVARCHAR(100) NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [ComplaintTheme] NVARCHAR(250) NOT NULL, 
    [ComplaintDateTime] DATETIME NOT NULL, 
    [PhoneCustomer] NVARCHAR(20) NOT NULL, 
    [ComplaintComment] NVARCHAR(MAX) NULL, 
    [ComplaintInformation] NVARCHAR(MAX) NULL, 
    [UpdatedOn] DATETIME NULL, 
    [UpdatedBy] NVARCHAR(100) NULL, 
    [RecordCallId] NVARCHAR(20) NULL, 
    [Operator] NVARCHAR(100) NULL, 
    [ComplaintResult] NVARCHAR(150) NULL, 
    [Responsible] NVARCHAR(150) NULL, 
    [Comment] NVARCHAR(MAX) NULL, 
    [CampaignID] INT NOT NULL, 
    [TypeCustomer] NVARCHAR(50) NOT NULL
)
