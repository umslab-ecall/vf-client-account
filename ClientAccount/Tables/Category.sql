﻿CREATE TABLE [dbo].[Category]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
    [ParentId] INT NULL, 
    [Name] NVARCHAR(250) NOT NULL, 
    [Url] NVARCHAR(MAX) NOT NULL, 
    [Order] INT NULL, 
    [PictureUrl] NVARCHAR(250) NULL, 
    [Active] BIT NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [UpdatedOn] DATETIME NULL, 
    [UpdatedBy] NVARCHAR(50) NULL
)
