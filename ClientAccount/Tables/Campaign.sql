﻿CREATE TABLE [dbo].[Campaign]
(
	[CampaignID] INT NOT NULL PRIMARY KEY, 
    [CampaignName] NVARCHAR(32) NOT NULL, 
    [Active] BIT NOT NULL, 
    [Deleted] BIT NOT NULL, 
    [Description] NVARCHAR(255) NULL, 
    [PrefixDigits] NVARCHAR(5) NULL
)
