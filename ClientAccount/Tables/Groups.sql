﻿CREATE TABLE [dbo].[Groups]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
    [Name] NVARCHAR(100) NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [LogoUrl] NVARCHAR(MAX) NULL, 
    [Active] BIT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [ModifiedOn] DATETIME NULL, 
    [ModifiedBy] NVARCHAR(50) NULL, 
    [CampaignID] INT NULL
)
