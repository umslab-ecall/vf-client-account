﻿CREATE TABLE [dbo].[Operators]
(
	[RSId] [int] NOT NULL,
	[SkillTargetID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[PeripheralNumber] [nvarchar](32) NOT NULL,
	[FirstName] [nvarchar](32) NOT NULL,
	[LastName] [nvarchar](32) NOT NULL
)
