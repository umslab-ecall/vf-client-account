﻿CREATE TABLE [dbo].[Campaign_SkillGroup]
(
	[CampaignID] INT NOT NULL , 
    [CampaignTypeId] INT NOT NULL, 
    [SkillTargetID] INT NOT NULL, 
    [SkillsetID] INT NULL, 
    [EnterpriseName] NVARCHAR(50) NOT NULL
)
