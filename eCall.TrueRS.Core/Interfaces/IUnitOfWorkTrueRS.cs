﻿using System;

namespace eCall.TrueRS.Core.Interfaces
{
    public interface IUnitOfWorkTrueRS : IDisposable
    {
        IVoiceRecord VoiceRecords { get; }
        IAgentRecord AgentRecords { get; }
        IRSStorage RSStorages { get; }
        void Commit();
    }
}
