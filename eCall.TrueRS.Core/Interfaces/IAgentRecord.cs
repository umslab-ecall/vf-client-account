﻿using eCall.TrueRS.Core.Entities;
using System.Collections.Generic;

namespace eCall.TrueRS.Core.Interfaces
{
    public interface IAgentRecord
    {
        IList<AgentRecord> AllAgentBySG(string listsg);
        IList<AgentRecord> AllAgentBySG(int sgId);
        IList<AgentRecord> AllAgentBySG(int[] sgId);
        IList<AgentRecord> AllAgentBySG(IList<int> sgId);
        IList<AgentRecord> AllAgentBySG(string startd, string endd, string listsg);
    }
}
