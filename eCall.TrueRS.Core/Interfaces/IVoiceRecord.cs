﻿using eCall.TrueRS.Core.Entities;
using System.Collections.Generic;

namespace eCall.TrueRS.Core.Interfaces
{
    public interface IVoiceRecord
    {
        IList<VoiceRecord> AllRecordByCustomer(string startd, string endd, string sgid, string phone, string agentid, int top, int? MinDuration, int? MaxDuration);
        IList<VoiceRecord> AllRecordByCustomer(string startd, string endd, string sgid, string agentid, int top1, int? MinDuration, int? MaxDuration);
        IList<Record> GetRecordById(int id);
        string GetRecordFileNameById(int id);
        void UpdateRecordstIDById(int Id, int stID);
    }
}
