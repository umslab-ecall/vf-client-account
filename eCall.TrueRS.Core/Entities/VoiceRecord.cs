﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.TrueRS.Core.Entities
{
    public class VoiceRecord
    {
        public int vrID { get; set; }
        public string Agent { get; set; }
        public string SkillGroup { get; set; }
        public DateTime StartTime { get; set; }
        public int? Duration { get; set; }
        public int? Hold { get; set; }
        public string ANI { get; set; }
        public string DNIS { get; set; }
        public string TN { get; set; }
        public string FileName { get; set; }        
    }
}
