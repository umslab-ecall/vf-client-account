﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.TrueRS.Core.Entities
{
    public class AgentRecord
    {
        public int SkillTargetID { get; set; }
        public string EnterpriseName { get; set; }
    }
}
