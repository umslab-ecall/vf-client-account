﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.TrueRS.Core.Entities
{
    public class RSStorage
    {
        public int stID { get; set; }
        public string Path { get; set; }
    }
}
