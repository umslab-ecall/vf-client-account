﻿using Dapper;
using eCall.TrueRS.Core.Entities;
using eCall.TrueRS.Core.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace eCall.TrueRS.Core.Repositories
{
    internal class AgentRecordRepository : RepositoryBase, IAgentRecord
    {
        public AgentRecordRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<AgentRecord> AllAgentBySG(string listsg)
        {
            var sql = @"
    Select distinct u.PeripheralNumber as SkillTargetID, u.[LastName] + ' ' + u.[FirstName] as EnterpriseName 
    from [rs].[dbo].[vrVoiceRecords] vr with (index(IX_vrVoiceRecords_SkillsetID))
    inner join [rs].[dbo].[rsUsers] u on vr.agId = u.[PeripheralNumber]

    WHERE vr.SkillsetID in ("+ listsg + ")";


            return Connection.Query<AgentRecord>(sql,
                //param: new { ListSG = listsg },
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }

        public IList<AgentRecord> AllAgentBySG(string startd, string endd, string listsg)
        {
            var sql = @"
    Select distinct u.PeripheralNumber as SkillTargetID, u.[LastName] + ' ' + u.[FirstName] as EnterpriseName 
    from [rs].[dbo].[vrVoiceRecords] vr with (index(IX_vrVoiceRecords_StartTime))
    inner join [rs].[dbo].[rsUsers] u on vr.agId = u.[PeripheralNumber]
    where StartTime between @StartDate and @EndDate
    WHERE vr.SkillsetID in (" + listsg + ")";


            return Connection.Query<AgentRecord>(sql,
                param: new { StartDate = startd, EndDate = endd, },
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }

        public IList<AgentRecord> AllAgentBySG(int sgId)
        {
            var sql = @"
                         Select distinct u.UID as SkillTargetID, u.[LastName] + ' ' + u.[FirstName] as EnterpriseName 
 from [rs].[dbo].[vrVoiceRecords] vr
 inner join [rs].[dbo].[rsGroups] g on vr.SkillsetID = g.grpID and g.UID = @SGId
 inner join [rs].[dbo].[rsUsers] u on vr.agId = u.[PeripheralNumber]";


            return Connection.Query<AgentRecord>(sql,
                param: new { SGId = sgId },
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }

        public IList<AgentRecord> AllAgentBySG(int[] sgId)
        {
            var sql = @"
                         Select distinct u.UID as SkillTargetID, u.[LastName] + ' ' + u.[FirstName] as EnterpriseName 
 from [rs].[dbo].[vrVoiceRecords] vr
 inner join [rs].[dbo].[rsGroups] g on vr.SkillsetID = g.grpID and g.UID in @SGId
 inner join [rs].[dbo].[rsUsers] u on vr.agId = u.[PeripheralNumber]";

            var parameters = new { SGId = new[] { sgId } };

            return Connection.Query<AgentRecord>(sql,
                parameters,
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }

        public IList<AgentRecord> AllAgentBySG(IList<int> sgId)
        {
            var sql = @"
                         Select distinct u.UID as SkillTargetID, u.[LastName] + ' ' + u.[FirstName] as EnterpriseName 
 from [rs].[dbo].[vrVoiceRecords] vr
 inner join [rs].[dbo].[rsGroups] g on vr.SkillsetID = g.grpID and g.UID in @SGId
 inner join [rs].[dbo].[rsUsers] u on vr.agId = u.[PeripheralNumber]";

            var parameters = new { SGId = sgId };

            return Connection.Query<AgentRecord>(sql,
                parameters,
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }
    }
}
