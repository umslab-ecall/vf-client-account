﻿using Dapper;
using eCall.TrueRS.Core.Entities;
using eCall.TrueRS.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.TrueRS.Core.Repositories
{
    internal class RSStorageRepository : RepositoryBase, IRSStorage
    {
        public RSStorageRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<RSStorage> GetStorageAll()
        {
            var sql = @"select stID, Path from rsStorages";


            return Connection.Query<RSStorage>(
                sql,
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }
    }
}
