﻿using Dapper;
using eCall.TrueRS.Core.Entities;
using eCall.TrueRS.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.TrueRS.Core.Repositories
{
    internal class VoiceRecordRepository : RepositoryBase, IVoiceRecord
    {
        public VoiceRecordRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<VoiceRecord> AllRecordByCustomer(string startd, string endd, string sgid, string phone, string agentid, int top, int? MinDuration, int? MaxDuration)
        {
            var sql = @"SELECT TOP " + top + @" vr.vrID, u.LastName + ' ' + u.FirstName as Agent, g.Name as SkillGroup
	                        ,vr.StartTime as StartTime
	                        ,vr.Duration as Duration
	                        ,vr.CLID as ANI
	                        ,vr.DNIS as DNIS
	                        ,vr.TN as TN
	                        ,Hold = (select sum(Duration) from dbo.vrVoiceRecords where vrParentID = vr.vrID)
	                        ,[FileName] = (select TOP 1 [FileName] from dbo.vrVoiceRecords where vrParentID = vr.vrID)
                        from dbo.vrVoiceRecords vr with (index(IX_vrVoiceRecords_StartTime))
                        left join dbo.rsGroups g on vr.SkillsetID = g.grpID
                        left join dbo.rsUsers u on vr.agId = u.[PeripheralNumber]
                        where StartTime between @StartDate and @EndDate
	                        and RecordID > 0
                        and vr.SkillsetID in (" + sgid + @" )
	                        and vr.agId in (" + agentid + @")
                        and (DNIS LIKE @Phone or CLID LIKE @Phone)";

            if ((MinDuration != null) && (MaxDuration != null))
            {
                sql += "and Duration between " + MinDuration + " and " + MaxDuration;
            }
            if ((MinDuration == null) && (MaxDuration != null))
            {
                sql += "and Duration <= " + MaxDuration;
            }
            if ((MinDuration == null) && (MaxDuration != null))
            {
                sql += "and Duration >= " + MinDuration;
            }

            return Connection.Query<VoiceRecord>(sql,
                param: new { StartDate = startd, EndDate = endd, SGId = sgid, AgentId = agentid, Phone = phone },
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }

        public IList<VoiceRecord> AllRecordByCustomer(string startd, string endd, string sgid, string agentid, int top, int? MinDuration, int? MaxDuration)
        {
            var sql = @"SELECT TOP " + top + @" vr.vrID, u.LastName + ' ' + u.FirstName as Agent, g.Name as SkillGroup
	                        ,vr.StartTime as StartTime
	                        ,vr.Duration as Duration
	                        ,vr.CLID as ANI
	                        ,vr.DNIS as DNIS
	                        ,vr.TN as TN
	                        ,Hold = (select sum(Duration) from dbo.vrVoiceRecords where vrParentID = vr.vrID)
	                        ,[FileName] = (select TOP 1 [FileName] from dbo.vrVoiceRecords where vrParentID = vr.vrID)
                        from dbo.vrVoiceRecords vr with (index(IX_vrVoiceRecords_StartTime))
                        left join dbo.rsGroups g on vr.SkillsetID = g.grpID
                        left join dbo.rsUsers u on vr.agId = u.[PeripheralNumber]
                        where StartTime between @StartDate and @EndDate
	                        and RecordID > 0
                        and vr.SkillsetID in (" + sgid + @" )
	                        and vr.agId in (" + agentid + ")";

            if((MinDuration != null) && (MaxDuration != null))
            {
                sql += "and Duration between " + MinDuration + " and " + MaxDuration;
            }
            if ((MinDuration == null) && (MaxDuration != null))
            {
                sql += "and Duration <= " + MaxDuration;
            }
            if ((MinDuration != null) && (MaxDuration == null))
            {
                sql += "and Duration >= " + MinDuration;
            }


            return Connection.Query<VoiceRecord>(sql,
                param: new { StartDate = startd, EndDate = endd },
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }

        public IList<Record> GetRecordById(int id)
        {
            var sql = @"SELECT 
                                vrID, 
                                StartTime, 
                                Duration, 
                                (select [Path] from [rs].[dbo].rsStorages where stID = vr.stID)+'\\'+[FileName] as FilePath, 
                                CLID+' -> '+DNIS as Direction,  
                                Agent = (select LastName+' '+FirstName Agent  
                                        from [rs].[dbo].rsUsers 
                                         where PeripheralNumber = agID),
                                SkillGroup = (select Name 
                                         from [rs].[dbo].rsGroups 
                                         where grpID = SkillsetID), 
                                CLID as ANI,
                                DNIS, 
                                TN 
                        FROM [rs].[dbo].vrVoiceRecords vr 
                        where (vrParentID = @RecordId or vrID = @RecordId) 
                        order by vrID";


            return Connection.Query<Record>(sql,
                param: new { RecordId = id },
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }

        public string GetRecordFileNameById(int id)
        {
            var sql = @"select top 1 FileName from dbo.vrVoiceRecords where vrParentID = @Id";

            return Connection.Query<string>(sql,
                param: new { Id = id },
                transaction: Transaction,
                commandTimeout: 620
               ).FirstOrDefault();
        }

        public void UpdateRecordstIDById(int Id, int stID)
        {
            var sql = @"update vrVoiceRecords set stID = @stID where vrID = @Id;
                        update vrVoiceRecords set stID = @stID where vrParentID  = @Id";

            Connection.Execute(sql,
                param: new { Id, stID },
                transaction: Transaction,
                commandTimeout: 620
               );
        }
    }
}
