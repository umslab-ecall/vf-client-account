﻿using eCall.TrueRS.Core.Interfaces;
using System;
using System.Data;
using System.Data.SqlClient;

namespace eCall.TrueRS.Core.Repositories
{
    public class UnitOfWorkTrueRS : IUnitOfWorkTrueRS
    {
        #region Fields

        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private IAgentRecord _agentRecordreRepository;
        private IVoiceRecord _voiceRecordRepository;
        private IRSStorage _rsStorageRepository;
        private bool _disposed;

        #endregion

        #region Constructor

        public UnitOfWorkTrueRS(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        #endregion

        #region Methods

        public IAgentRecord AgentRecords
        {
            get { return _agentRecordreRepository ?? (_agentRecordreRepository = new AgentRecordRepository(_transaction)); }
        }

        public IVoiceRecord VoiceRecords
        {
            get { return _voiceRecordRepository ?? (_voiceRecordRepository = new VoiceRecordRepository(_transaction)); }
        }

        public IRSStorage RSStorages
        {
            get { return _rsStorageRepository ?? (_rsStorageRepository = new RSStorageRepository(_transaction)); }
        }

        #endregion

        #region Commit

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                resetRepositories();
                SqlConnection.ClearAllPools();
            }
        }

        #endregion

        #region ResetRepositories

        private void resetRepositories()
        {
            _agentRecordreRepository = null;
            _voiceRecordRepository = null;
            _rsStorageRepository = null;
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitOfWorkTrueRS()
        {
            dispose(false);
        }

        #endregion
    }
}
