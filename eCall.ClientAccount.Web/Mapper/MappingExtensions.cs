﻿using eCall.ClientAccount.Services.DTO.Catalog;
using eCall.ClientAccount.Services.DTO.Complaints;
using eCall.ClientAccount.Services.DTO.Statistics;
using eCall.ClientAccount.Services.DTO.TrueRS;
using eCall.ClientAccount.Web.Models.CallRecords;
using eCall.ClientAccount.Web.Models.Catalog;
using eCall.ClientAccount.Web.Models.Complaint;
using eCall.ClientAccount.Web.Models.Statistics;

namespace eCall.ClientAccount.Web.Mapper
{
    public static class MappingExtensions
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return AutoMapperConfiguration.Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return AutoMapperConfiguration.Mapper.Map(source, destination);
        }

        #region VoiceRecord

        public static VoiceRecordModel ToModel(this VoiceRecordDTO entity)
        {
            return entity.MapTo<VoiceRecordDTO, VoiceRecordModel>();
        }

        public static VoiceRecordDTO ToEntity(this VoiceRecordModel model)
        {
            return model.MapTo<VoiceRecordModel, VoiceRecordDTO>();
        }

        public static VoiceRecordDTO ToEntity(this VoiceRecordModel model, VoiceRecordDTO destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Category

        public static CategoryModel ToModel(this CategoryDTO entity)
        {
            return entity.MapTo<CategoryDTO, CategoryModel>();
        }

        public static CategoryDTO ToEntity(this CategoryModel model)
        {
            return model.MapTo<CategoryModel, CategoryDTO>();
        }

        public static CategoryDTO ToEntity(this CategoryModel model, CategoryDTO destination)
        {
            return model.MapTo(destination);
        }

        #endregion
        
        #region Record

        public static RecordModel ToModel(this RecordDTO entity)
        {
            return entity.MapTo<RecordDTO, RecordModel>();
        }

        public static RecordDTO ToEntity(this RecordModel model)
        {
            return model.MapTo<RecordModel, RecordDTO>();
        }

        public static RecordDTO ToEntity(this RecordModel model, RecordDTO destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region MP3Record

        public static MP3RecordModel ToModel(this MP3RecordDTO entity)
        {
            return entity.MapTo<MP3RecordDTO, MP3RecordModel>();
        }

        public static MP3RecordDTO ToEntity(this MP3RecordModel model)
        {
            return model.MapTo<MP3RecordModel, MP3RecordDTO>();
        }

        public static MP3RecordDTO ToEntity(this MP3RecordModel model, MP3RecordDTO destination)
        {
            return model.MapTo(destination);
        }

        #endregion
        
        #region ReportCategory

        public static ReportCategoryModel ToModel(this ReportCategoryDTO entity)
        {
            return entity.MapTo<ReportCategoryDTO, ReportCategoryModel>();
        }

        public static ReportCategoryDTO ToEntity(this ReportCategoryModel model)
        {
            return model.MapTo<ReportCategoryModel, ReportCategoryDTO>();
        }

        public static ReportCategoryDTO ToEntity(this ReportCategoryModel model, ReportCategoryDTO destination)
        {
            return model.MapTo(destination);
        }


        #endregion

        #region StatisticsModel

        public static StatisticsModel ToModel(this StatisticsDTO entity)
        {
            return entity.MapTo<StatisticsDTO, StatisticsModel>();
        }

        public static StatisticsDTO ToEntity(this StatisticsModel model)
        {
            return model.MapTo<StatisticsModel, StatisticsDTO>();
        }

        public static StatisticsDTO ToEntity(this StatisticsModel model, StatisticsDTO destination)
        {
            return model.MapTo(destination);
        }


        #endregion

        #region StatisticsOutStatusModel

        public static StatisticsOutStatusModel ToModel(this StatisticsOutStatusDTO entity)
        {
            return entity.MapTo<StatisticsOutStatusDTO, StatisticsOutStatusModel>();
        }

        public static StatisticsOutStatusDTO ToEntity(this StatisticsOutStatusModel model)
        {
            return model.MapTo<StatisticsOutStatusModel, StatisticsOutStatusDTO>();
        }

        public static StatisticsOutStatusDTO ToEntity(this StatisticsOutStatusModel model, StatisticsOutStatusDTO destination)
        {
            return model.MapTo(destination);
        }


        #endregion

        #region StatisticsOutStatusCallModel

        public static StatisticsOutStatusCallModel ToModel(this StatisticsOutStatusCallDTO entity)
        {
            return entity.MapTo<StatisticsOutStatusCallDTO, StatisticsOutStatusCallModel>();
        }

        public static StatisticsOutStatusCallDTO ToEntity(this StatisticsOutStatusCallModel model)
        {
            return model.MapTo<StatisticsOutStatusCallModel, StatisticsOutStatusCallDTO>();
        }

        public static StatisticsOutStatusCallDTO ToEntity(this StatisticsOutStatusCallModel model, StatisticsOutStatusCallDTO destination)
        {
            return model.MapTo(destination);
        }


        #endregion

        #region StatisticsOutStatusNotCallModel

        public static StatisticsOutStatusNotCallModel ToModel(this StatisticsOutStatusNotCallDTO entity)
        {
            return entity.MapTo<StatisticsOutStatusNotCallDTO, StatisticsOutStatusNotCallModel>();
        }

        public static StatisticsOutStatusNotCallDTO ToEntity(this StatisticsOutStatusNotCallModel model)
        {
            return model.MapTo<StatisticsOutStatusNotCallModel, StatisticsOutStatusNotCallDTO>();
        }

        public static StatisticsOutStatusNotCallDTO ToEntity(this StatisticsOutStatusNotCallModel model, StatisticsOutStatusNotCallDTO destination)
        {
            return model.MapTo(destination);
        }


        #endregion
        
        #region ComplaintModel

        public static ComplaintModel ToModel(this ComplaintDTO entity)
        {
            return entity.MapTo<ComplaintDTO, ComplaintModel>();
        }

        public static ComplaintDTO ToEntity(this ComplaintModel model)
        {
            return model.MapTo<ComplaintModel, ComplaintDTO>();
        }

        public static ComplaintDTO ToEntity(this ComplaintModel model, ComplaintDTO destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region ComplaintAddModel

        public static ComplaintAddModel ToModelAdd(this ComplaintDTO entity)
        {
            return entity.MapTo<ComplaintDTO, ComplaintAddModel>();
        }

        public static ComplaintDTO ToEntityAdd(this ComplaintAddModel model)
        {
            return model.MapTo<ComplaintAddModel, ComplaintDTO>();
        }

        public static ComplaintDTO ToEntityAdd(this ComplaintAddModel model, ComplaintDTO destination)
        {
            return model.MapTo(destination);
        }

        #endregion

    }
}