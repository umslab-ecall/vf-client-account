﻿using AutoMapper;
using eCall.ClientAccount.Services.DTO.Catalog;
using eCall.ClientAccount.Services.DTO.Complaints;
using eCall.ClientAccount.Services.DTO.Statistics;
using eCall.ClientAccount.Services.DTO.TrueRS;
using eCall.ClientAccount.Web.Models.CallRecords;
using eCall.ClientAccount.Web.Models.Catalog;
using eCall.ClientAccount.Web.Models.Complaint;
using eCall.ClientAccount.Web.Models.Statistics;

namespace eCall.ClientAccount.Web.Mapper
{
    public static class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            MapperConfiguration = new MapperConfiguration(cfg =>
            {
                // ----- VoiceRecord -----
                cfg.CreateMap<VoiceRecordDTO, VoiceRecordModel>();
                cfg.CreateMap<VoiceRecordModel, VoiceRecordDTO>();

                // ----- Category -----
                cfg.CreateMap<CategoryDTO, CategoryModel>();
                cfg.CreateMap<CategoryModel, CategoryDTO>();

                //------ Record ------
                cfg.CreateMap<RecordDTO, RecordModel>();
                cfg.CreateMap<RecordModel, RecordDTO>();

                // ------------ MP3Record -----------
                cfg.CreateMap<MP3RecordDTO, MP3RecordModel>();
                cfg.CreateMap<MP3RecordModel, MP3RecordDTO>();

                // ----- ReportCategory ------
                cfg.CreateMap<ReportCategoryDTO, ReportCategoryModel>();
                cfg.CreateMap<ReportCategoryModel, ReportCategoryDTO>();

                // ------------ StatisticsModel -----------
                cfg.CreateMap<StatisticsDTO, StatisticsModel>();
                cfg.CreateMap<StatisticsModel, StatisticsDTO>();





                // ------------ StatisticsOutStatusModel -----------
                cfg.CreateMap<StatisticsOutStatusDTO, StatisticsOutStatusModel>();
                cfg.CreateMap<StatisticsOutStatusModel, StatisticsOutStatusDTO>();

                // ------------ StatisticsOutStatusCallModel -----------
                cfg.CreateMap<StatisticsOutStatusCallDTO, StatisticsOutStatusCallModel>();
                cfg.CreateMap<StatisticsOutStatusCallModel, StatisticsOutStatusCallDTO>();

                // ------------ StatisticsOutStatusNotCallModel -----------
                cfg.CreateMap<StatisticsOutStatusNotCallDTO, StatisticsOutStatusNotCallModel>();
                cfg.CreateMap<StatisticsOutStatusNotCallModel, StatisticsOutStatusNotCallDTO>();


                // ------------ ComplaintModel -----------
                cfg.CreateMap<ComplaintDTO, ComplaintModel>();
                cfg.CreateMap<ComplaintModel, ComplaintDTO>();

                // ------------ ComplaintAddModel -----------
                cfg.CreateMap<ComplaintDTO, ComplaintAddModel>();
                cfg.CreateMap<ComplaintAddModel, ComplaintDTO>();
            });
            Mapper = MapperConfiguration.CreateMapper();
        }
        public static IMapper Mapper { get; private set; }

        public static MapperConfiguration MapperConfiguration { get; private set; }
    }
}