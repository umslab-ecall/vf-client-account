﻿using eCall.ClientAccount.Web.Filter;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CultureAttribute());
        }
    }
}
