﻿using eCall.ClientAccount.Services.Services.Catalog;
using eCall.ClientAccount.Services.Services.Complaints;
using eCall.ClientAccount.Services.Services.FileManager;
using eCall.ClientAccount.Services.Services.Reports;
using eCall.ClientAccount.Services.Services.Security;
using eCall.ClientAccount.Services.Services.TrueRS;
using Ninject.Modules;

namespace eCall.ClientAccount.Web.Infrastructure.NinjectUtil
{
    public class CAModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ISkillGroupRecordService>().To<SkillGroupRecordService>();
            Bind<ICategoryService>().To<CategoryService>();
            Bind<IReportService>().To<ReportService>();
            Bind<IAccessService>().To<AccessService>();
            Bind<ISkillGroupReportService>().To<SkillGroupReportService>();
            Bind<ICallTypeReportService>().To<CallTypeReportService>();
            Bind<IStatisticsReportService>().To<StatisticsReportService>();
            Bind<IComplaintService>().To<ComplaintService>();
            Bind<IFileManagerService>().To<FileManagerService>();
        }
    }
}