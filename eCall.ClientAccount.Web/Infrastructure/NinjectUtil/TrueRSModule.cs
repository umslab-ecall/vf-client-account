﻿using eCall.ClientAccount.Services.Services.TrueRS;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCall.ClientAccount.Web.Infrastructure.NinjectUtil
{
    public class TrueRSModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAgentRecordService>().To<AgentRecordService>();
            Bind<IVoiceRecordService>().To<VoiceRecordService>();
        }
    }
}