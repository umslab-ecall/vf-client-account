﻿using eCall.ClientAccount.Services.Services.IPCC;
using Ninject.Modules;

namespace eCall.ClientAccount.Web.Infrastructure.NinjectUtil
{
    public class IPCCModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ISkillGroupService>().To<SkillGroupService>();            
        }
    }
}