﻿using eCall.ClientAccount.Services.Services.Security;
using Ninject.Modules;

namespace eCall.ClientAccount.Web.Infrastructure.NinjectUtil
{
    public class SecurityModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IActiveDirectoryService>().To<ActiveDirectoryService>();
            Bind<IAuthenticationService>().To<OwinAuthenticationService>();
            Bind<IDomainService>().To<DomainService>();
            Bind<IUserService>().To<UserService>();
            Bind<IWebUserSession>().To<UserSession>();
        }
    }
}