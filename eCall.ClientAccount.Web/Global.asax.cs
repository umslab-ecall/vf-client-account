﻿using eCall.ClientAccount.Services.Infrastructure.ServiceModule;
using eCall.ClientAccount.Services.Mapper;
using eCall.ClientAccount.Web.Infrastructure.NinjectUtil;
using eCall.ClientAccount.Web.Mapper;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace eCall.ClientAccount.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ConfigureAntiForgeryTokens();
            AutoMapperConfiguration.Initialize();
            AutoMapperServicesConfiguration.Initialize();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;

            // внедрение зависимостей
            NinjectModule securityModule = new SecurityModule();
            NinjectModule ipccModule = new IPCCModule();
            NinjectModule truersModule = new TrueRSModule();
            NinjectModule caModule = new CAModule();

            NinjectModule ecall_identityModule = new eCall_IdentityModule(ConfigurationManager.ConnectionStrings["IdentityConnection"].ConnectionString);
            NinjectModule ecall_ipccModule = new eCall_IPCCModule(ConfigurationManager.ConnectionStrings["IPCCConnection"].ConnectionString);
            NinjectModule ecall_truersModule = new eCall_TrueRSModule(ConfigurationManager.ConnectionStrings["TrueRSConnection"].ConnectionString);
            NinjectModule ecall_caModule = new eCall_ClientAccountModule(ConfigurationManager.ConnectionStrings["ClientAccount"].ConnectionString);

            var kernel = new StandardKernel(securityModule, ecall_identityModule, ipccModule, ecall_ipccModule, truersModule, ecall_truersModule, caModule, ecall_caModule);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }

        private void ConfigureAntiForgeryTokens()
        {
            // We use Owin Cookie Authentication, so we set AntiForgery token to use Name as claim types.
            // If you use different claim type, you need to synchronize with 
            // AspNetMvcActiveDirectoryOwin.Web.Common.Security.OwinAuthenticationService.
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;

            // Anti-Forgery cookie requires SSL to be sent across the wire. 
            //AntiForgeryConfig.RequireSsl = true;
        }
    }
}
