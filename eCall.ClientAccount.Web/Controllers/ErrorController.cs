﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Controllers
{
    public class ErrorController : Controller
    {
        //требуется авторизация
        public ActionResult Unauthorized()
        {
            Response.StatusCode = 401;
            return View();
        }

        //не найдено
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        //доступ запрещен
        public ActionResult Forbidden()
        {
            Response.StatusCode = 403;
            return View();
        }

        //время отклика истекло
        public ActionResult RequestTimeout()
        {
            Response.StatusCode = 408;
            return View();
        }

        //недоступен
        public ActionResult Gone()
        {
            Response.StatusCode = 410;
            return View();
        }

        //внутренняя ошибка сервера
        public ActionResult InternalServerError()
        {
            Response.StatusCode = 500;
            return View();
        }

        //неверный шлюз
        public ActionResult BadGateway()
        {
            Response.StatusCode = 502;
            return View();
        }

        //служба временно недоступна
        public ActionResult ServiceUnavailable()
        {
            Response.StatusCode = 503;
            return View();
        }

        //время ответа сервера истекло
        public ActionResult GatewayTimeout()
        {
            Response.StatusCode = 505;
            return View();
        }
    }
}