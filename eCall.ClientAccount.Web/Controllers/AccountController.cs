﻿using eCall.ClientAccount.Services.Services.Security;
using eCall.ClientAccount.Web.Models.Account;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Controllers
{
    public class AccountController : Controller
    {
        #region Fields

        private readonly IActiveDirectoryService _activeDirectoryService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IDomainService _domainService;
        private readonly IUserService _userService;

        #endregion

        #region Constructors

        public AccountController(
            IActiveDirectoryService activeDirectoryService,
            IAuthenticationService authenticationService,
            IDomainService domserv,
            IUserService userService
            )
        {
            _activeDirectoryService = activeDirectoryService;
            _authenticationService = authenticationService;
            _domainService = domserv;
            _userService = userService;
        }

        #endregion

        #region Methods

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            var model = new LoginModel { AvailableDomains = GetDomains() };
            return View("Login", model);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                bool result = _activeDirectoryService.ValidateCredentials(model.Domain, model.UserName, model.Password);
                if (result)
                {
                    var user = _userService.GetUserByUserName(model.UserName);
                    if (user != null && user.Active)
                    {
                        //var test = await _userService.GetRoleByUser(user.Id);

                        var roleNames = user.Roles.Select(r => r.Name).ToList();
                        _authenticationService.SignIn(user, roleNames);

                        // Update LastLoginDate for future reference
                        //user.LastLoginDate = _dateTime.Now;
                        //await _userService.UpdateUserAsync(user);
                        
                        // Redirect to return URL
                        if (!string.IsNullOrEmpty(returnUrl) && !string.Equals(returnUrl, "/") && Url.IsLocalUrl(returnUrl))
                            return RedirectToLocal(returnUrl);

                        // User is in a role, so redirect to Administration area
                        if (roleNames.Contains("Developer") ||
                            roleNames.Contains("Application Manager"))
                            //return RedirectToRoute("Dashboard");
                            return RedirectToAction("Index", "Home");

                        return RedirectToAction("Index", "Home");
                    }
                    ModelState.AddModelError("", "You does not have access to Application. Please contact your manager.");
                }
                else
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
            }
            model.AvailableDomains = GetDomains();
            return View("Login", model);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            _authenticationService.SignOut();
            return RedirectToAction("Login", "Account");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }


        #endregion

        #region Util

        private IList<SelectListItem> GetDomains()
        {
            var domains = _domainService.GetAllDomains();

            return domains
                .Select(d => new SelectListItem { Text = d.Name, Value = d.Name })
                .ToList();
        }

        #endregion
    }
}