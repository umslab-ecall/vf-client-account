﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCall.Moodle;
using eCall.ClientAccount.Web.Models.Grades;

namespace eCall.ClientAccount.Web.Controllers
{
    public class GradeController : Controller
    {
        private readonly MoodleApi _api;
        public GradeController()
        {
            _api = new MoodleApi(
                ConfigurationManager.AppSettings["moodle.login"]
                ,ConfigurationManager.AppSettings["moodle.pass"]
                ,ConfigurationManager.AppSettings["moodle.token"]
                ,ConfigurationManager.AppSettings["moodle.url"]
                );
        }
        public ActionResult MoodleReport(string id, string categoryId, string cohortId)
        {
            var model = new MoodleReportViewModel();

            model.CategoryId = categoryId;
            model.CohortId = cohortId;
            model.CourseId = id;
            model.DateFrom = DateTime.Today.AddDays(-10);
            model.DateTo = DateTime.Today;

            if (!string.IsNullOrEmpty(categoryId))
            {
                var coursers = _api.GetCourses(categoryId);
                model.Courses = coursers.OrderBy(x=> x.FullName).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.FullName }).ToList();
            }

            if (!string.IsNullOrEmpty(cohortId))
            {
                var users = _api.GetUsers(cohortId, true);
                model.Users = users.OrderBy(x=> x.FullName).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.FullName }).ToList();
                model.Users.Insert(0, new SelectListItem() { Value = "-1", Text = "---" });
            }

            return View("MoodleReport", model);
        }

        public ActionResult GetMoodleReport(MoodleReportViewModel model)
        {
            IList<MoodleReportGradeItem> list;
            if (!string.IsNullOrEmpty(model.CourseId))
                list = _api.GetGradeItems(model.CourseId);
            else
                list = new List<MoodleReportGradeItem>();

            list = list.ToList().FindAll(x =>
                                (!model.HasUserId || model.UserId == x.UserId) &&
                                (!model.HasDates || (x.Completed >= model.DateFrom && x.Completed <= model.DateTo))).ToList();

            return View("_MoodleReport", list);
        }

        public string GetMoodleAttempt(string id)
        {
            var res = _api.GetAttemptHtml(id);
            return res;
        }
    }
}