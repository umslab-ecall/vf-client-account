﻿using eCall.ClientAccount.Web.Filter;
using eCall.ClientAccount.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Controllers
{
    [ClientAuthorize(Roles = "CAManager, CAUser, CAAdmin")]
    public class CommonController : Controller
    {
        #region Fields

        #endregion

        #region Constructors

        #endregion

        #region Methods

        //logo
        [ChildActionOnly]
        public virtual ActionResult Logo()
        {
            string src = Url.Content("~/Content/layouts/layout/img/Logo_red.png");
            //var model = _commonModelFactory.PrepareLogoModel();
            var model = new LogoModel { LogoPath = src, StoreName = "eBilling" };

            return PartialView(model);
        }

        //language
        [ChildActionOnly]
        public virtual ActionResult LanguageSelector()
        {
            //var model = _commonModelFactory.PrepareLanguageSelectorModel();

            //if (model.AvailableLanguages.Count == 1)
            //    Content("");

            return PartialView(/*model*/);
        }

        //language        
        public virtual ActionResult ChangeCulture(string lang)
        {
            //var model = _commonModelFactory.PrepareLanguageSelectorModel();

            //if (model.AvailableLanguages.Count == 1)
            //    Content("");

            //return PartialView(/*model*/);
            string returnUrl = Request.UrlReferrer.AbsolutePath;
            // Список культур
            List<string> cultures = new List<string>() { "ru", "uk" };
            if (!cultures.Contains(lang))
            {
                lang = "uk";
            }
            // Сохраняем выбранную культуру в куки
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
                cookie.Value = lang;   // если куки уже установлено, то обновляем значение
            else
            {

                cookie = new HttpCookie("lang");
                cookie.HttpOnly = false;
                cookie.Value = lang;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return Redirect(returnUrl);
        }

        //footer
        [ChildActionOnly]
        public virtual ActionResult Footer()
        {
            //var model = _commonModelFactory.PrepareFooterModel();
            return PartialView(/*model*/);
        }

        //favicon
        [ChildActionOnly]
        public virtual ActionResult Favicon()
        {
            var model = "~/favicon.ico";
            //var model = _commonModelFactory.PrepareFaviconModel();
            //if (String.IsNullOrEmpty(model.FaviconUrl))
            //    return Content("");

            return PartialView(model);
        }

        #endregion
    }
}