﻿using eCall.ClientAccount.Services.DTO.Statistics;
using eCall.ClientAccount.Services.Services.Reports;
using eCall.ClientAccount.Services.Services.Security;
using eCall.ClientAccount.Services.Services.TrueRS;
using eCall.ClientAccount.Web.Mapper;
using eCall.ClientAccount.Web.Models.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Controllers
{
    public class StatisticController : Controller
    {
        #region Fields

        private readonly IWebUserSession _webUserSession;
        private readonly IAccessService _accessService;
        private readonly ICallTypeReportService _callTypeReportService;
        private readonly ISkillGroupReportService _skillGroupReportService;
        private readonly IStatisticsReportService _statisticsReportService;

        #endregion

        #region Constructor

        public StatisticController(
            IWebUserSession webUserSession,
            IAccessService accessserv,
            ICallTypeReportService ctserv,
            ISkillGroupReportService sgserv,
            IStatisticsReportService statserv)
        {
            _webUserSession = webUserSession;
            _accessService = accessserv;
            _callTypeReportService = ctserv;
            _skillGroupReportService = sgserv;
            _statisticsReportService = statserv;
        }

        #endregion

        #region Methods

        // GET: Statistic
        public ActionResult StatisticInb()
        {
            if (_accessService.AccessCategory(_webUserSession.Id, HttpContext.Request.RawUrl))
            {
                return RedirectToAction("Forbidden", "Error");
            }

            StatisticsFindModel model = new StatisticsFindModel
            {
                AvailableSkillGroup = GetAvailableSkillGroup(_webUserSession.Id, "Inbound")
            };

            return View(model);
        }

        [HttpPost]
        // GET: Statistic
        public ActionResult StatisticInbSearch(string sd, string ed, string SkillGroupId)
        {
            var statisticsDTO = _statisticsReportService.GetStatisticsInbSearch(sd, ed, SkillGroupId, _webUserSession.Id, "Inbound");
            var model = statisticsDTO.Select(x => x.ToModel()).ToList();
          
            return PartialView(model);
        }

        public ActionResult StatisticOut()
        {
            if (_accessService.AccessCategory(_webUserSession.Id, HttpContext.Request.RawUrl))
            {
                return RedirectToAction("Forbidden", "Error");
            }

            return View();
        }

        public ActionResult StatisticOutSearch(string sd, string ed)
        {
            List<StatisticsOutStatusCallDTO> liststatusCallDTO;
            List<StatisticsOutStatusDTO> liststatusDTO;
            List<StatisticsOutStatusNotCallDTO> liststatusNotCallDTO;

            _statisticsReportService.GetStatisticsOut(sd, ed, out liststatusCallDTO, out liststatusDTO, out liststatusNotCallDTO);

            ViewBag.StatusCall = liststatusCallDTO.Select(x => x.ToModel()).ToList();
            ViewBag.Statu = liststatusDTO.Select(x => x.ToModel()).ToList();
            ViewBag.StatusNotCall = liststatusNotCallDTO.Select(x => x.ToModel()).ToList();

            return PartialView();
        }

        //GetStatisticsOut

        #endregion

        #region Util

        private IList<SelectListItem> GetAvailableSkillGroup(int userId, string type)
        {
            var SkillGroup = new List<SelectListItem>();
            var sg = (_skillGroupReportService.GetSGRecordForCustomer(userId, type))
                .Select(d => new SelectListItem { Text = d.EnterpriseName, Value = d.SkillTargetID.ToString() })
                .ToList();
            if (sg.Count == 0)
                SkillGroup.Add(new SelectListItem { Text = "Нет доступных SkillGroup", Value = "", Selected = true });
            else
            {
                SkillGroup.Add(new SelectListItem { Text = "Все SkillGroup", Value = "-1", Selected = true });
                SkillGroup.AddRange(sg);
            }
            return SkillGroup;
        }

        #endregion
    }
}