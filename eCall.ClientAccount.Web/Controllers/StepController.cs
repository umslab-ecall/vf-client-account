﻿using eCall.ClientAccount.Services.Services.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Controllers
{
    public class StepController : Controller
    {
        #region Fields

        private readonly IWebUserSession _webUserSession;
        private readonly IAccessService _accessService;

        #endregion

        #region Constructor

        public StepController(
            IWebUserSession webUserSession,
            IAccessService accessserv)
        {
            _webUserSession = webUserSession;
            _accessService = accessserv;
        }

        #endregion

        #region Methods

        // GET: Step
        public ActionResult Index()
        {
            if (_accessService.AccessCategory(_webUserSession.Id, HttpContext.Request.RawUrl))
            {
                return RedirectToAction("Forbidden", "Error");
            }

            return View();
        }

        #endregion

    }
}