﻿using eCall.ClientAccount.Services.DTO.Catalog;
using eCall.ClientAccount.Services.Services.Catalog;
using eCall.ClientAccount.Services.Services.Security;
using eCall.ClientAccount.Web.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Controllers
{
    public class CatalogController : Controller
    {
        #region Fields

        private readonly IWebUserSession _webUserSession;
        private readonly ICategoryService _categoryService;

        #endregion

        #region Constructor

        public CatalogController(
            IWebUserSession webUserSession, 
            ICategoryService catsrv)
        {
            _webUserSession = webUserSession;
            _categoryService = catsrv;            
        }

        #endregion

        #region Methods

        // GET: Catalog
        public ActionResult Index()
        {
            return View();
        }

        //category
        [ChildActionOnly]
        public ActionResult Category()
        {
            IEnumerable<CategoryDTO> categpryDtos = _categoryService.GetAllCategoryForCustomer(_webUserSession.Id);
            var category = categpryDtos.Select(x => x.ToModel()).ToList();

            IEnumerable<ReportCategoryDTO> reportDtos = _categoryService.GetAllReportCategoryForCustomer(_webUserSession.Id);            
            var report = reportDtos.Select(x => x.ToModel()).ToList();

            ViewBag.reportForCustomer = report.ToList();

            return PartialView(category);
        }

        #endregion
    }
}