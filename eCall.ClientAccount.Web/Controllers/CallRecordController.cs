﻿using eCall.ClientAccount.Services.Services.IPCC;
using eCall.ClientAccount.Services.Services.Security;
using eCall.ClientAccount.Services.Services.TrueRS;
using eCall.ClientAccount.Web.Filter;
using eCall.ClientAccount.Web.Mapper;
using eCall.ClientAccount.Web.Models.CallRecords;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Controllers
{
    [ClientAuthorize(Roles = "CAManager, CAUser, CAAdmin")]
    public class CallRecordController : Controller
    {
        #region Fields

        private readonly IWebUserSession _webUserSession;
        private readonly ISkillGroupRecordService _skillGroupRecordService;
        private readonly IAgentRecordService _agentRecordService;
        private readonly IVoiceRecordService _voiceRecordService;
        private readonly IAccessService _accessService;

        #endregion

        #region Constructor

        public CallRecordController(
            IWebUserSession webUserSession,
            ISkillGroupRecordService sgserv,
            IAgentRecordService agentserv,
            IVoiceRecordService vrserv,
            IAccessService accessserv)
        {
            _webUserSession = webUserSession;
            _skillGroupRecordService = sgserv;
            _agentRecordService = agentserv;
            _voiceRecordService = vrserv;
            _accessService = accessserv;
        }

        #endregion

        #region Methods

        public ActionResult RecordIn()
        {
            if(_accessService.AccessCategory(_webUserSession.Id, HttpContext.Request.RawUrl))
            {
                return RedirectToAction("Forbidden", "Error");
            }

            var model = new FindRecordModel
            {
                AvailableSkillGroup = GetAvailableSkillGroup(_webUserSession.Id, "Inbound"),
                AvailableAgent = GetAvailableAgent(_webUserSession.Id, "Inbound")
            };
            return View("RecordIn", model);
        }

        public ActionResult RecordOut()
        {
            if (_accessService.AccessCategory(_webUserSession.Id, HttpContext.Request.RawUrl))
            {
                return RedirectToAction("Forbidden", "Error");
            }

            var model = new FindRecordModel
            {
                AvailableSkillGroup = GetAvailableSkillGroup(_webUserSession.Id, "Outbound"),
                AvailableAgent = GetAvailableAgent(_webUserSession.Id, "Outbound")
            };
            return View("RecordOut", model);
        }

        [HttpPost]
        public ActionResult RecordOutSearch(FindRecordModel findrecord)
        {
            var voiceDto = _voiceRecordService.GetRecordByCustomer(Convert.ToDateTime(findrecord.StartDate), Convert.ToDateTime(findrecord.EndDate), findrecord.SkillGroupId.ToString(), findrecord.Phone, findrecord.AgentId.ToString(), findrecord.Top, _webUserSession.Id, "Outbound", findrecord.MinDuration, findrecord.MaxDuration);
            var model = voiceDto.Select(x => x.ToModel()).ToList();

            return PartialView("RecordOutSearch", model);
        }

        [HttpPost]
        public ActionResult RecordInSearch(FindRecordModel findrecord)
        {
            var dateFrom = Convert.ToDateTime(findrecord.StartDate);
            var dateTo = Convert.ToDateTime(findrecord.EndDate); //.AddDays(1).AddMilliseconds(-1);
            var voiceDto = _voiceRecordService.GetRecordByCustomer(dateFrom, dateTo, findrecord.SkillGroupId.ToString(), findrecord.Phone, findrecord.AgentId.ToString(), findrecord.Top, _webUserSession.Id, "Inbound", findrecord.MinDuration, findrecord.MaxDuration);
            var model = voiceDto.Select(x => x.ToModel()).ToList();
            if (findrecord.EndCall != "All")
            {
                model = model.Where(x => x.EndCall == findrecord.EndCall).ToList();
            }
            //else
            //{
            //    var model = voiceDto.Select(x => x.ToModel()).Where(x => x.EndCall == findrecord.EndCall).ToList();
            //}            

            return PartialView("RecordInSearch", model);
        }

        public ActionResult Details(int id)
        {
            //var recordDto = _voiceRecordService.GetRecordById(id, Server.MapPath("~/Media"), Request.Browser.Browser);
            //var model = recordDto.Select(x => x.ToModel()).ToList();

            var recordDto = _voiceRecordService.GetMP3RecordById(id, Server.MapPath("~/Media"), Request.Browser.Browser);
            
            var model = recordDto.ToModel();
            return PartialView(model);
        }

        //public PartialViewResult RepairIn(string id)
        //{
        //    //string ID = GetSTID(id);
        //    if (id != null && id != "")
        //    {
        //        //SQL.Update(" update vrVoiceRecords set stID=" + ID + " where vrID=" + id);
        //        //SQL.Update(" update vrVoiceRecords set stID=" + ID + " where vrParentID=" + id);
        //    }
        //    return PartialView("Details", new { id });
        //}

        #endregion

        #region Util

        private IList<SelectListItem> GetAvailableSkillGroup(int userId, string type)
        {
            var SkillGroup = new List<SelectListItem>();
            var sg = (_skillGroupRecordService.GetSGRecordForCustomer(userId, type))
                .Select(d => new SelectListItem { Text = d.EnterpriseName, Value = d.SkillsetID.ToString() })
                .ToList();
            if (sg.Count == 0)
                SkillGroup.Add(new SelectListItem { Text = "Нет доступных SkillGroup", Value = "", Selected = true });
            else
            {
                SkillGroup.Add(new SelectListItem { Text = "Все SkillGroup", Value = "-1", Selected = true });
                SkillGroup.AddRange(sg);
            }                
            return SkillGroup;
        }

        private IList<SelectListItem> GetAvailableAgent(int userId, string type)
        {
            var Agent = new List<SelectListItem>();

            var agent = (_agentRecordService.GetAgentBySG(userId, type))
                .Select(d => new SelectListItem { Text = d.EnterpriseName, Value = d.SkillTargetID.ToString() })
                .ToList();
            if (agent.Count == 0)
                Agent.Add(new SelectListItem { Text = "Нет доступных операторов", Value = "", Selected = true });
            else
            {
                Agent.Add(new SelectListItem { Text = "Все операторы", Value = "-1", Selected = true });
                Agent.AddRange(agent);
            }
                
            return Agent;
        }

        #endregion        
    }
}