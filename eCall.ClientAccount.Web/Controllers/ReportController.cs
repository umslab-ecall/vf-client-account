﻿using eCall.ClientAccount.Services.Services.Complaints;
using eCall.ClientAccount.Services.Services.Reports;
using eCall.ClientAccount.Services.Services.Security;
using eCall.ClientAccount.Web.Filter;
using eCall.ClientAccount.Web.Mapper;
using eCall.ClientAccount.Web.Models.Complaint;
using eCall.ClientAccount.Web.Models.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Xml;
using System.Xml.Serialization;

namespace eCall.ClientAccount.Web.Controllers
{
    [ClientAuthorize(Roles = "CAManager, CAUser, CAAdmin")]
    public class ReportController : Controller
    {
        #region Fields

        private readonly IReportService _reportService;
        private readonly IWebUserSession _webUserSession;
        private readonly IAccessService _accessService;
        private readonly ISkillGroupReportService _skillGroupReportService;
        private readonly IComplaintService _complaintService;

        #endregion

        #region Constructor

        public ReportController(
            IReportService repserv,
            IWebUserSession webUserSession,
            IAccessService accessserv,
            ISkillGroupReportService sgserv,
            IComplaintService complaintserv)
        {
            _reportService = repserv;
            _webUserSession = webUserSession;
            _accessService = accessserv;
            _skillGroupReportService = sgserv;
            _complaintService = complaintserv;
        }

        #endregion

        #region Methods

        public ActionResult Index(int reportId)
        {
            if (_accessService.AccessReport(_webUserSession.Id, reportId))
            {
                return RedirectToAction("Forbidden", "Error");
            }

            var model = new FindReportModel
            {
                ReportId = reportId,
                AvailableSkillGroup = GetAvailableSkillGroup(_webUserSession.Id)
            };

            return View(model);
        }

        // GET: ReportDisplay
        public JsonResult ReportDisplay(int? reportId, string skillgroupid, string StartDate, string EndDate)
        {
            if (reportId == null)
                return Json("", JsonRequestBehavior.AllowGet);

            var jsondata = new List<Dictionary<string, object>>();

            jsondata = _reportService.ExecuteReport((int)reportId, skillgroupid, StartDate, EndDate, _webUserSession.Id);
            
            var jsonResult = Json(jsondata, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult Complaint()
        {
            //var complaintDto = _complaintService.GetAllComplaint(_webUserSession.Id);
            //var model = complaintDto.Select(x => x.ToModel()).ToList();

            return View(/*model*/);
        }

        public ActionResult ComplaintSearch(string sd, string ed, string phone, string rescomplaint)
        {
            var complaintDto = _complaintService.GetAllComplaint(_webUserSession.Id, sd, ed, phone, rescomplaint);
            var model = complaintDto.Select(x => x.ToModel()).ToList();

            return PartialView(model);
        }

        [HttpGet]
        public ActionResult DetailsComplaint(int Id)
        {
            var complaintDto = _complaintService.GetComplaintById(Id, _webUserSession.Id);
            var model = complaintDto.ToModel();

            return View(model);
        }

        [HttpGet]
        public ActionResult AddComplaint()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddComplaint(ComplaintAddModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var complaintDto = model.ToEntityAdd();
            _complaintService.CreateComplaint(complaintDto, _webUserSession.Id);
            
            return RedirectToAction("Complaint"); 
        }

        [ClientAuthorize(Roles = "CAManager")]
        [HttpGet]
        public ActionResult EditComplaint(int Id)
        {
            var complaintDto = _complaintService.GetComplaintById(Id, _webUserSession.Id);
            var model = complaintDto.ToModel();

            return View(model);
        }

        [ClientAuthorize(Roles = "CAManager")]
        [HttpPost]
        public ActionResult EditComplaint(ComplaintModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var complaintDto = model.ToEntity();
            _complaintService.UpdateComplaint(complaintDto, _webUserSession.Id);

            return RedirectToAction("Complaint"); 
        }

        
        #endregion

        #region Util

        private IList<SelectListItem> GetAvailableSkillGroup(int userId)
        {
            var SkillGroup = new List<SelectListItem>();
            var sg = (_skillGroupReportService.GetSGRecordForCustomer(userId))
                .Select(d => new SelectListItem { Text = d.EnterpriseName, Value = d.SkillTargetID.ToString() })
                .ToList();
            if (sg.Count == 0)
                SkillGroup.Add(new SelectListItem { Text = "Нет доступных SkillGroup", Value = "", Selected = true });
            else
            {
                SkillGroup.Add(new SelectListItem { Text = "Все SkillGroup", Value = "-1", Selected = true });
                SkillGroup.AddRange(sg);
            }
            return SkillGroup;
        }
        

        #endregion
    }
}