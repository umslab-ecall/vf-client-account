﻿using eCall.ClientAccount.Services.Services.FileManager;
using eCall.ClientAccount.Services.Services.Security;
using eCall.ClientAccount.Web.Models.FileManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Controllers
{
    public class FileManagerController : Controller
    {
        #region Fields

        private readonly IWebUserSession _webUserSession;
        private readonly IAccessService _accessService;
        private readonly IFileManagerService _fileManagerService;

        #endregion

        #region Constructor

        public FileManagerController(
            IWebUserSession webUserSession,
            IAccessService accessserv,
            IFileManagerService fileserv)
        {
            _webUserSession = webUserSession;
            _accessService = accessserv;
            _fileManagerService = fileserv;
        }

        #endregion

        #region Methods

        // GET: FileManager
        public ActionResult Index()
        {
            if (_accessService.AccessCategory(_webUserSession.Id, HttpContext.Request.RawUrl))
            {
                return RedirectToAction("Forbidden", "Error");
            }
            var uploadedFiles = new List<UploadedFileModel>();

            var fileDto = _fileManagerService.Find(10, 0, Server.MapPath("~/SharedFiles"));

            foreach (var file in fileDto)
            {
                var fileInfo = new System.IO.FileInfo(file);

                var uploadedFile = new UploadedFileModel() { Name = System.IO.Path.GetFileName(file) };

                long sizef = fileInfo.Length;
                uploadedFile.Size = sizef / 1024;
                uploadedFile.CreationDateTime = fileInfo.CreationTime;

                uploadedFile.Path = ("SharedFiles/") + System.IO.Path.GetFileName(file);
                uploadedFiles.Add(uploadedFile);
            }

            return View(uploadedFiles);
        }

        public FileResult Download(string file)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/SharedFiles/") + file);
            string fileName = file;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                // получаем имя файла
                string fileName = System.IO.Path.GetFileName(upload.FileName);
                // сохраняем файл в папку Files в проекте
                upload.SaveAs(Server.MapPath("~/SharedFiles/" + fileName));
            }
            return RedirectToAction("Index");
        }

        public ActionResult CreateFolder()
        {

            return View();
        }



        #endregion
    }
}