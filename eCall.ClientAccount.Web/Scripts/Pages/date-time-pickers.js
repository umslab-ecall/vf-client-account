﻿var ComponentsDateTimePickers = function () {


    var handleDateRangePickers = function () {
        if (!jQuery().daterangepicker) {
            return;
        }

        $('#reportrange').daterangepicker({
            //"ranges": {
            //    'Сегодня': [moment(), moment()],
            //    'Вчера': [moment().subtract('days', 1), moment().subtract('days', 1)],
            //    'Последние 7 дней': [moment().subtract('days', 6), moment()],
            //    'Последние 30 дней': [moment().subtract('days', 29), moment()],
            //    'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
            //    'Прошлый месяц': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            //},
            "opens": (App.isRTL() ? 'right' : 'left'),
            "startDate": moment(),
            "endDate": moment(),
            //minDate: '01/01/2012',
            //maxDate: '12/31/2014',
            //"dateLimit": {
            //    "days": 60
            //},
            "showDropdowns": true,
            "showWeekNumbers": true,
            "timePicker": false,
            "timePickerIncrement": 1,
            "timePicker12Hour": true,

            "buttonClasses": ['btn'],
            "applyClass": 'green',
            "cancelClass": 'default',
            "format": 'D.MM.YYYY',
            "separator": ' to ',
            "locale": {
                "applyLabel": 'Принять',
                "cancelLabel": 'Отмена',
                "fromLabel": 'From',
                "toLabel": 'To',
                "customRangeLabel": 'Диапазон',
                "daysOfWeek": ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                "monthNames": ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октября', 'Ноябрь', 'Декабрь'],
                "firstDay": 1
            }
        },
            function (start, end) {
                $('#reportrange span').html(start.format('D.MM.YYYY') + ' - ' + end.format('D.MM.YYYY'));
            }
        );

        //Set the initial state of the picker label
        $('#reportrange span').html(moment().format('D.MM.YYYY') + ' - ' + moment().format('D.MM.YYYY'));
        $('#reportrange').show();
    }

    var handleDatetimePicker = function () {

        if (!jQuery().datetimepicker) {
            return;
        }

        $(".form_datetime").datetimepicker({
            //autoclose: true,
            //isRTL: App.isRTL(),
            //format: "dd.mm.yyyy hh:ii",
            //format: 'DD.MM.YYYY HH:mm',
            //pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
            useCurrent: false,
            locale: moment.locale(),
            format: 'DD.MM.YYYY HH:mm'
        });

        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal

        // Workaround to fix datetimepicker position on window scroll
        $(document).scroll(function () {
            $('#form_modal1 .form_datetime, #form_modal1 .form_advance_datetime, #form_modal1 .form_meridian_datetime').datetimepicker('place'); //#modal is the id of the modal
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleDatetimePicker();
            handleDateRangePickers();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ComponentsDateTimePickers.init();
    });
}