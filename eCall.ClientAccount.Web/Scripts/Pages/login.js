var Login = function() {

    var handleLogin = function() {
        function format(state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><img src="../Content/global/img/domain/' + state.element.value.toLowerCase() + '.ico" class="img-flag" /> ' + state.text + '</span>'
            );

            return $state;
        }

        // basic
        $('#Domain_list, #Domain_list_validate').select2({
            placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Domain',
            templateResult: format,
            templateSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });

        //if (jQuery().select2 && $('#Domain_list').size() > 0) {
        //    $("#Domain_list").select2({
        //        placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Domain',
        //        templateResult: format,
        //        templateSelection: format,
        //        width: 'auto',
        //        escapeMarkup: function (m) {
        //            return m;
        //        }
        //    });


        //    $('#Domain_list').change(function () {
        //        $('.login-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        //    });
        //}
        

        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                Domain: {
                    required: true
                },
                UserName: {
                    required: true
                },
                Password: {
                    required: true
                },
                Remember: {
                    required: false
                }
            },

            messages: {
                username: {
                    required: "Username is required."
                },
                password: {
                    required: "Password is required."
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });

       

        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });

        
    }
   

    return {
        //main function to initiate the module
        init: function() {

            handleLogin();

        }

    };

}();

jQuery(document).ready(function() {
    Login.init();
});