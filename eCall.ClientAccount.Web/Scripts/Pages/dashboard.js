﻿var Dashboard = function () {

    return {
    
        initDashboardDaterange: function() {
            if (!jQuery().daterangepicker) {
                return;
            }
            
            $('#dashboard-report-range').daterangepicker({                
                ranges: {
                    'Сегодня': [moment(), moment()],
                    'Вчера': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Последние 7 дней': [moment().subtract('days', 6), moment()],
                    'Последние 30 дней': [moment().subtract('days', 29), moment()],
                    'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                    'Прошлый месяц': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                locale: {
                    "format": "MM/DD/YYYY",
                    "separator": " - ",
                    "applyLabel": "Ок",
                    "cancelLabel": "Отмена",
                    "fromLabel": "От",
                    "toLabel": "до",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": [
                        "Вс",
                        "Пн",
                        "Вт",
                        "Ср",
                        "Чт",
                        "Пт",
                        "Сб"
                    ],
                    "monthNames": [
                        "Январь",
                        "Февраль",
                        "Март",
                        "Апрель",
                        "Май",
                        "Июнь",
                        "Июль",
                        "Август",
                        "Сентябрь",
                        "Октября",
                        "Ноябрь",
                        "Декабрь"
                    ],
                    firstDay: 0
                },
                //"startDate": "11/08/2015",
                //"endDate": "11/14/2015",
                opens: (App.isRTL() ? 'right' : 'left'),
            }, function(start, end, label) {
                $('#dashboard-report-range span').html(start.format('D.MM.YYYY') + ' - ' + end.format('D.MM.YYYY'));
            });

            $('#dashboard-report-range span').html(moment().format('D.MM.YYYY') + ' - ' + moment().format('D.MM.YYYY'));
            $('#dashboard-report-range').show();

            $('#dashboard-report-range').on('apply.daterangepicker', function (ev, picker) {
                var sDate = picker.startDate.format('YYYY-MM-DD');
                var eDate = picker.endDate.format('YYYY-MM-DD');
                $.ajax({
                    type: 'POST',
                    url: '/Candidates/GetData',
                         data: {
                        'sd': sDate, 'ed': eDate
                    },
                    //cache: false,
                    success: function (html) {
                        $("table#sample_1 tbody").html(html);


                    }
                });
                //var sDate = picker.startDate.format('YYYY-MM-DD');
                //var eDate = picker.endDate.format('YYYY-MM-DD');
                
                //$.ajax({
                //    type: 'GET',
                //    url: '/Candidates/ResponseList',
                //    data: {
                //        'sd': sDate, 'ed': eDate
                //    },
                //    success: function (response) { // response is returned by server 
                //        $('table#sample_1 tbody').load("/Candidates/GetData");  // <------ Here you add rows updated on table
                //    }
                //    //dataType: 'json',
                //    //success: function (data) {
                //    //    console.log(data);
                //    //}
                //});
            });
        },               
        init: function() {            
            this.initDashboardDaterange();
        }
    };
}();

    jQuery(document).ready(function() {
        Dashboard.init(); // init metronic core componets        
    });
