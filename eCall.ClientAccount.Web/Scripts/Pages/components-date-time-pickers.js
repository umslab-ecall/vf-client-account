var sDate = '';
var eDate = '';

var ComponentsDateTimePickers = function () {
    

    var handleDateRangePickers = function () {
        if (!jQuery().daterangepicker) {
            return;
        }

        $('#reportrange').daterangepicker({
            "ranges": { 
                '�������': [moment(), moment()],
                '�����': [moment().subtract('days', 1), moment().subtract('days', 1)],
                '��������� 7 ����': [moment().subtract('days', 6), moment()],
                '��������� 30 ����': [moment().subtract('days', 29), moment()],
                '���� �����': [moment().startOf('month'), moment().endOf('month')],
                '������� �����': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            "opens": (App.isRTL() ? 'right' : 'left'),
                "startDate": moment().subtract('days', 29),
                "endDate": moment(),
                //minDate: '01/01/2012',
                //maxDate: '12/31/2014',
                "dateLimit": {
                    "days": 60
                },
                "showDropdowns": true,
                "showWeekNumbers": true,
                "timePicker": false,
                "timePickerIncrement": 1,
                "timePicker12Hour": true,
                
                "buttonClasses": ['btn'],
                "applyClass": 'green',
                "cancelClass": 'default',
                "format": 'D.MM.YYYY',
                "separator": ' to ',
                "locale": {
                    "applyLabel": '�������',
                    "fromLabel": 'From',
                    "toLabel": 'To',
                    "customRangeLabel": 'Custom Range',
                    "daysOfWeek": ['��', '��', '��', '��', '��', '��', '��'],
                    "monthNames": ['������', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    "firstDay": 1
                }
            },
            function (start, end) {
                $('#reportrange span').html(start.format('D.MM.YYYY') + ' - ' + end.format('D.MM.YYYY'));
            }
        );

        //Set the initial state of the picker label
        $('#reportrange span').html(moment().subtract('days', 29).format('D.MM.YYYY') + ' - ' + moment().format('D.MM.YYYY'));
        $('#reportrange').show();
    }

    var handleDatetimePicker = function () {

        if (!jQuery().datetimepicker) {
            return;
        }

        $(".form_datetime").datetimepicker({
            //autoclose: true,
            //isRTL: App.isRTL(),
            //format: "dd.mm.yyyy hh:ii",
            //fontAwesome: true,
            //pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
            useCurrent: true,
            keepOpen: false,
            locale: moment.locale(),
            format: 'DD.MM.YYYY HH:mm:ss' //'DD.MM.YYYY HH:mm'
        });

        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal

        // Workaround to fix datetimepicker position on window scroll
        $( document ).scroll(function(){
            $('#form_modal1 .form_datetime, #form_modal1 .form_advance_datetime, #form_modal1 .form_meridian_datetime').datetimepicker('place'); //#modal is the id of the modal
        });

        $('.form_datetime').datetimepicker().on('dp.change', function (ev) {
            $(this).data("DateTimePicker").hide();
        }); 

        $("#datefrom").on("dp.change", function (e) {
            //var d = e.date.startOf('day');
            //console.log(d);
            sDate = $("#datefrom-input").val();
            //$('#dateto').data("DateTimePicker").minDate(e.date);
        });
        $("#dateto").on("dp.change", function (e) {
            //var d = e.date.startOf('day').add(1, 'day').subtract(1, 'second');
            //console.log(d);
            eDate = $("#dateto-input").val();
            //$('#datefrom').data("DateTimePicker").maxDate(e.date);
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleDatetimePicker();
            handleDateRangePickers();
        }
    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {    
        ComponentsDateTimePickers.init(); 
    });
}