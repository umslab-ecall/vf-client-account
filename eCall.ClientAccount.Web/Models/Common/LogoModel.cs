﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCall.ClientAccount.Web.Models.Common
{
    public class LogoModel
    {
        public string StoreName { get; set; }
        public string LogoPath { get; set; }
    }
}