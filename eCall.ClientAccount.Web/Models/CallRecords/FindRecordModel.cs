﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Models.CallRecords
{
    public class FindRecordModel
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int? SkillGroupId { get; set; }
        public string Phone { get; set; }
        public int? AgentId { get; set; }
        public int? Top { get; set; }
        public int? MinDuration { get; set; }
        public int? MaxDuration { get; set; }
        public string EndCall { get; set; }
        public IList<SelectListItem> AvailableSkillGroup { get; set; }
        public IList<SelectListItem> AvailableAgent { get; set; }
        public FindRecordModel()
        {
            AvailableSkillGroup = new List<SelectListItem>();
            AvailableAgent = new List<SelectListItem>();
        }
    }
}