﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace eCall.ClientAccount.Web.Models.CallRecords
{
    public class RecordModel
    {
        public int vrID { get; set; }
        [Display(Name = "Оператор")]
        public string Agent { get; set; }
        [Display(Name = "SkillGroup", ResourceType = typeof(Resource))]
        public string SkillGroup { get; set; }
        [Display(Name = "StartTime", ResourceType = typeof(Resource))]
        public DateTime StartTime { get; set; }
        [Display(Name = "Duration", ResourceType = typeof(Resource))]
        public int? Duration { get; set; }
        public int? Hold { get; set; }
        [Display(Name = "PhoneNumber", ResourceType = typeof(Resource))]
        public string ANI { get; set; }
        public string DNIS { get; set; }
        public string TN { get; set; }
        [Display(Name = "FilePath", ResourceType = typeof(Resource))]
        public string FilePath { get; set; }
        [Display(Name = "Direction", ResourceType = typeof(Resource))]
        public string Direction { get; set; }
        public string EndCall { get; set; }
    }
}