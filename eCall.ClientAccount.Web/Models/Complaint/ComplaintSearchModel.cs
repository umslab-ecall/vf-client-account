﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Models.Complaint
{
    public class ComplaintSearchModel
    {
        [Display(Name = "PeriodSelection", ResourceType = typeof(Resource))]
        public string PeriodSelection { get; set; }

        [Display(Name = "Номер телефона клиента")]
        public string PhoneCustomer { get; set; }

        [Display(Name = "Результат обработки жалобы")]
        public string ComplaintResult { get; set; }
    }
}