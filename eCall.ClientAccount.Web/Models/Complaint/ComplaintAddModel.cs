﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCall.ClientAccount.Web.Models.Complaint
{
    public class ComplaintAddModel
    {
        public int ComplaintId { get; set; }
        public int CampaignID { get; set; }
        [Display(Name = "Описание жалобы")]
        public string ComplaintDescription { get; set; }
        [Display(Name = "Кем создано")]
        public string CreatedBy { get; set; }
        [Display(Name = "Когда создано")]
        public DateTime? CreatedOn { get; set; }
        [Display(Name = "Тема жалобы")]
        public string ComplaintTheme { get; set; }
        [Display(Name = "Дата и время поступления жалобы")]
        public DateTime? ComplaintDateTime { get; set; }
        [Display(Name = "Номер телефона клиента")]
        public string PhoneCustomer { get; set; }
        [Display(Name = "Тип абонента")]
        public string TypeCustomer { get; set; }
        [Display(Name = "Коментарии по жалобе")]
        public string ComplaintComment { get; set; }
        [Display(Name = "Информация по жалобе")]
        public string ComplaintInformation { get; set; }
    }
}