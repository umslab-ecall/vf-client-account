﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eCall.ClientAccount.Web.Models.Complaint
{
    public class ComplaintDetailsModel
    {
        public int ComplaintId { get; set; }
        public int CampaignID { get; set; }



        [Display(Name = "Описание жалобы")]
        public string ComplaintDescription { get; set; }
        [Display(Name = "Кем создано")]
        public string CreatedBy { get; set; }
        [Display(Name = "Когда создано")]
        public DateTime? CreatedOn { get; set; }
        [Display(Name = "Тема жалобы")]
        public string ComplaintTheme { get; set; }
        [Display(Name = "Дата и время поступления жалобы")]
        public DateTime? ComplaintDateTime { get; set; }
        [Display(Name = "Номер телефона клиента")]
        public string PhoneCustomer { get; set; }
        [Display(Name = "Коментарии по жалобе")]
        public string ComplaintComment { get; set; }
        [Display(Name = "Информация по жалобе")]
        public string ComplaintInformation { get; set; }



        [Display(Name = "Дата и время обработки жалобы")]
        public DateTime UpdatedOn { get; set; }
        [Display(Name = "Кто обрабатывал запись")]
        public string UpdatedBy { get; set; }
        [Display(Name = "ID записи разговора")]
        public string RecordCallId { get; set; }
        [Display(Name = "Оператор")]
        public string Operator { get; set; }
        [Display(Name = "Результат обработки жалобы")]
        public string ComplaintResult { get; set; }
        [Display(Name = "Ответственный")]
        public string Responsible { get; set; }
        [Display(Name = "Коментарий жалобы")]
        public string Comment { get; set; }
    }
}