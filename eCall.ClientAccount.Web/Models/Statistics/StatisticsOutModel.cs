﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCall.ClientAccount.Web.Models.Statistics
{
    public class StatisticsOutStatusModel
    {
        public string CallStatus { get; set; }
        public string CountRec { get; set; }
        public string RoundRec { get; set; }
    }

    public class StatisticsOutStatusCallModel
    {
        public string Result { get; set; }
        public string CountRec { get; set; }
        public string RoundRec { get; set; }
    }

    public class StatisticsOutStatusNotCallModel
    {
        public string Result { get; set; }
        public string CountRec { get; set; }
        public string RoundRec { get; set; }
    }
}