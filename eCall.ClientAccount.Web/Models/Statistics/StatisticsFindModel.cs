﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Models.Statistics
{
    public class StatisticsFindModel
    {
        public string Period { get; set; }
        public int? SkillGroupId { get; set; }
        public IList<SelectListItem> AvailableSkillGroup { get; set; }
        public StatisticsFindModel()
        {
            AvailableSkillGroup = new List<SelectListItem>();
        }
    }
}