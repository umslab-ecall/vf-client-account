﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCall.ClientAccount.Web.Models.Catalog
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int Order { get; set; }
        public string PictureUrl { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }
    }
}