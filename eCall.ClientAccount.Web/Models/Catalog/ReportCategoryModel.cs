﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCall.ClientAccount.Web.Models.Catalog
{
    public class ReportCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public int Order { get; set; }
        public string PictureUrl { get; set; }
    }
}