﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCall.Moodle;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Models.Grades
{
    public class MoodleReportViewModel
    {
        public string CategoryId { get; set; }
        public string CohortId { get; set; }
        public IList<SelectListItem> Users { get; set; }
        public IList<SelectListItem> Courses { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public bool HasDates => DateFrom.HasValue && DateTo.HasValue;
        public string UserId { get; set; }
        public bool HasUserId => string.IsNullOrEmpty(UserId) || UserId == "-1" ? false : true;
        public string CourseId { get; set; }
        public bool HasCourseId => string.IsNullOrEmpty(CourseId) || CourseId == "-1" ? false : true;
    }
}