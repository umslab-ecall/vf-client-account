﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCall.ClientAccount.Web.Models.Reports
{
    public class FindReportModel
    {
        public int ReportId { get; set; }
        public string Period { get; set; }        
        public int? SkillGroupId { get; set; }
        public IList<SelectListItem> AvailableSkillGroup { get; set; }
        public FindReportModel()
        {
            AvailableSkillGroup = new List<SelectListItem>();
        }
    }
}