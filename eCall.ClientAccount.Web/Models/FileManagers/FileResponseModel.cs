﻿using System.Collections.Generic;

namespace eCall.ClientAccount.Web.Models.FileManagers
{
    public class FileResponseModel
    {
        public FileResponseModel()
        {
            Files = new List<FileInstanceModel>();
            Path = string.Empty;
        }
        public IEnumerable<FileInstanceModel> Files { get; set; }
        public string Path { get; set; }
    }
}