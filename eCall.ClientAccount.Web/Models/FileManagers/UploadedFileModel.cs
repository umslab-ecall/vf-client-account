﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCall.ClientAccount.Web.Models.FileManagers
{
    public class UploadedFileModel
    {
        public string Name { get; set; }
        public long Size { get; set; }
        public string Path { get; set; }
        public DateTime CreationDateTime { get; set; }
    }
}