﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCall.ClientAccount.Web.Models.FileManagers
{
    public class FileInstanceModel
    {
        public bool IsChecked { get; set; }
        public string Created { get; set; }
        public string Name { get; set; }
        public string FileSize { get; set; }
        public FileType FileType { get; set; }
        public string FullPath { get; set; }
    }

    public enum FileType
    {
        Directory,
        File,
        Image,
        None
    }
}