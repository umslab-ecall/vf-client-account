﻿if (typeof SegmentHtml != "undefined") {
    SegmentHtmlParam.prototype['filter'] = function () {
        var name = null;
        var value = null;
        for (var i = 1; i < this._tokens.length; i++) {
            var token = this._tokens[i];
            if (token.type === ATTR_NAME) {
                name = csco_g_buffer.substring(token.first_index, token.last_index).toUpperCase();
            } else if (token.type === ATTR_VALUE) {
                value = csco_g_buffer.substring(token.first_index, token.last_index);
            };
        };
        var need_processing = false;
        if (ParserClsidName) {
            var tmp = ParserClsidName[this._clsid];
            if (tmp) {
                var proc = tmp[name];
                need_processing = typeof proc != 'undefined';
            };
        };
        /**
        * ERROR ON NEXT LINE: name is null
        */
        if (name != null && name.toLowerCase() == "csco_proto") {
            this._parent['csco_proto'] = value;
        };
        if (need_processing) { this._parent[name] = value; };
    };
};