﻿using Dapper;
using eCall.Identity.Core.Entities.Domains;
using eCall.Identity.Core.Interfaces.Domains;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.Identity.Core.Repositories.Domains
{
    internal class DomainRepository : RepositoryBase, IDomain
    {
        public DomainRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<ADomain> GetAllDomainsRep()
        {
            var sql = @"
                SELECT *
                FROM dbo.Domain";


            return Connection.Query<ADomain>(sql,
                transaction: Transaction
               ).ToList();
        }
    }
}
