﻿using eCall.Identity.Core.Interfaces;
using eCall.Identity.Core.Interfaces.Customers;
using eCall.Identity.Core.Interfaces.Domains;
using eCall.Identity.Core.Repositories.Customers;
using eCall.Identity.Core.Repositories.Domains;
using System;
using System.Data;
using System.Data.SqlClient;

namespace eCall.Identity.Core.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private IUser _userRepository;
        private IDomain _domainRepository;        
        private bool _disposed;

        public UnitOfWork(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public IUser Users
        {
            get { return _userRepository ?? (_userRepository = new UserRepository(_transaction)); }
        }

        public IDomain Domains
        {
            get { return _domainRepository ?? (_domainRepository = new DomainRepository(_transaction)); }
        }
        

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                resetRepositories();
                SqlConnection.ClearAllPools();
            }
        }

        private void resetRepositories()
        {
            _userRepository = null;
            _domainRepository = null;            
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitOfWork()
        {
            dispose(false);
        }
    }
}
