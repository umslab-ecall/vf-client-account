﻿using Dapper;
using eCall.Identity.Core.Entities.Customers;
using eCall.Identity.Core.Interfaces.Customers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.Identity.Core.Repositories.Customers
{
    internal class UserRepository : RepositoryBase, IUser
    {
        public UserRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public Customer GetUserByUserNameAsync(string userName)
        {
            var sql = @"
                SELECT * 
                FROM dbo.Customer 
                WHERE UserName = @UserName";

            return Connection.Query<Customer>(sql,
                param: new { UserName = userName },
                transaction: Transaction
            ).FirstOrDefault();
        }

        public IList<Role> GetRolesAsync(Customer user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var sql = @"
                SELECT
                    IR.Name
                FROM
                    Role IR
                    INNER JOIN CustomerRoles IUR ON IR.Id = IUR.RoleId AND IUR.UserId=@UserId";


            return Connection.Query<Role>(sql,
                param: new { UserId = user.Id },
                transaction: Transaction
               ).ToList();
        }

        public string GetUserNameById(int customerId)
        {
            var sql = @"
                SELECT top 1 [UserName] 
                FROM dbo.Customer 
                WHERE Id = @Id";

            return Connection.Query<string>(sql,
                param: new { Id = customerId },
                transaction: Transaction
            ).FirstOrDefault();

        }
    }
}
