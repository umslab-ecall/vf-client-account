﻿using eCall.Identity.Core.Entities.Domains;
using System.Collections.Generic;

namespace eCall.Identity.Core.Interfaces.Domains
{
    public interface IDomain
    {
        IList<ADomain> GetAllDomainsRep();
    }
}
