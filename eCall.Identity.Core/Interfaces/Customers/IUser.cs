﻿using eCall.Identity.Core.Entities.Customers;
using System.Collections.Generic;

namespace eCall.Identity.Core.Interfaces.Customers
{
    public interface IUser
    {
        Customer GetUserByUserNameAsync(string userName);
        IList<Role> GetRolesAsync(Customer user);
        string GetUserNameById(int customerId);
    }
}
