﻿using eCall.Identity.Core.Interfaces.Customers;
using eCall.Identity.Core.Interfaces.Domains;
using System;

namespace eCall.Identity.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUser Users { get; }
        IDomain Domains { get; }       
        void Commit();
    }
}
