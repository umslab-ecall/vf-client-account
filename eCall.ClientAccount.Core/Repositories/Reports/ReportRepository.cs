﻿using Dapper;
using eCall.ClientAccount.Core.Entities.Reports;
using eCall.ClientAccount.Core.Interfaces.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace eCall.ClientAccount.Core.Repositories.Reports
{
    internal class ReportRepository : RepositoryBase, IReport
    {
        public ReportRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public List<Dictionary<string, object>> ExecutSP(string storedProcedure, string skillgroupid, string StartDate, string EndDate)
        {
            //var affectedRows = new List<object>();
            var affectedRows =  Connection.Query<dynamic>(storedProcedure,
            new { db = StartDate, de = EndDate, ctid = skillgroupid },
            commandType: CommandType.StoredProcedure,
            commandTimeout: 360,
            transaction: Transaction);

            var resultSet = new List<Dictionary<string, object>>();

            foreach (IDictionary<string, object> row in affectedRows)
            {
                var dict = new Dictionary<string, object>();
                foreach (var pair in row)
                {
                    if(pair.Value == null)
                    {
                        dict.Add(pair.Key, pair.Value);
                    }
                    else
                    {
                        dict.Add(pair.Key, pair.Value.ToString());
                    }
                    //dict.Add(pair.Key, pair.Value.ToString());
                }
                resultSet.Add(dict);
            }

            return resultSet;
        }

        public List<StatisticsSearch> StatisticsInb(string sd, string ed, string CallTypeID)
        {
            var sql = @"[dbo].[Report_StatisticsInb]";
            
            return Connection.Query<StatisticsSearch>(sql,
                param: new { db = sd, de = ed, ctid = CallTypeID },
                commandType: CommandType.StoredProcedure,
                commandTimeout: 600,
                transaction: Transaction
               ).ToList();
        }

        public List<StatisticsOutStatus> StatisticsOutStatus(string sd, string ed)
        {
            var sql = @"[dbo].[Report_StatisticsOutVodafone]";

            return Connection.Query<StatisticsOutStatus>(sql,
                param: new { db = sd, de = ed },
                commandType: CommandType.StoredProcedure,
                commandTimeout: 360,
                transaction: Transaction
               ).ToList();
        }

        public List<StatisticsOutStatusCall> StatisticsOutStatusCall(string sd, string ed)
        {
            var sql = @"[dbo].[Report_StatisticsOutStatusCall]";

            return Connection.Query<StatisticsOutStatusCall>(sql,
                param: new { db = sd, de = ed },
                commandType: CommandType.StoredProcedure,
                commandTimeout: 360,
                transaction: Transaction
               ).ToList();
        }

        public List<StatisticsOutStatusNotCall> StatisticsOutStatusNotCall(string sd, string ed)
        {
            var sql = @"[dbo].[Report_StatisticsOutStatusNotCall]";

            return Connection.Query<StatisticsOutStatusNotCall>(sql,
                param: new { db = sd, de = ed },
                commandType: CommandType.StoredProcedure,
                commandTimeout: 360,
                transaction: Transaction
               ).ToList();
        }

        public Report GetReportById(int reportId)
        {
            var sql = @"
                Select *
				From [dbo].[Reports]
				where [Id] = @ReportId";

            return Connection.Query<Report>(sql,
                param: new { ReportId = reportId },
                transaction: Transaction
               ).FirstOrDefault();
        }

        public string SkillGroupToCallType(string skillgroupid)
        {
            var sql = @"
                Select [CallTypeID]
				From [dbo].[SkillGroup_CallType]
				where [SkillTargetID] in (" + skillgroupid + ")";

            var res = Connection.Query<int>(sql,
                transaction: Transaction
               ).ToList();

            return String.Join(", ", res);
        }

        public string GetCompaingNameByCustomer(int customerid)
        {
            var sql = @"
                SELECT c.CampaignName
  FROM [ClientAccount].[dbo].[Campaign] c 
  inner join [ClientAccount].[dbo].[Groups] g on c.CampaignID = g.CampaignID
  inner join [ClientAccount].[dbo].[CustomerGroup] cg on g.Id = cg.GroupsId
  where cg.CustomerId = @CustomerId";

            return Connection.Query<string>(sql,
                param: new { CustomerId = customerid },
                transaction: Transaction
               ).FirstOrDefault();
        }
    }
}
