﻿using Dapper;
using eCall.ClientAccount.Core.Entities;
using eCall.ClientAccount.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Repositories
{
    internal class CustomerAttributeRepository : RepositoryBase, ICustomerAttribute
    {
        public CustomerAttributeRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<CustomerSG> AllSGForCustomer(int userId, string type)
        {
            var sql = @"
                        Select gsg.SkillTargetID
From dbo.Group_SkillGroup gsg
inner join dbo.GroupType gt on gsg.GroupTypeId = gt.Id and gt.Name = @GroupType
inner join dbo.Groups g on gsg.GroupsId = g.Id
inner join dbo.CustomerGroup cg on g.Id = cg.GroupsId and cg.CustomerId = @CustomerId";


            return Connection.Query<CustomerSG>(sql,
                param: new { GroupType = type, CustomerId = userId },
                transaction: Transaction
               ).ToList();
        }
    }
}
