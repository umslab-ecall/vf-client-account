﻿using Dapper;
using eCall.ClientAccount.Core.Entities.Complaints;
using eCall.ClientAccount.Core.Interfaces.Complaints;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace eCall.ClientAccount.Core.Repositories.Complaints
{
    /// <summary>
    /// Complaint repository
    /// </summary>
    internal class ComplaintRepository : RepositoryBase, IComplaint
    {
        public ComplaintRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        /// <summary>
        /// Get complaint list
        /// </summary>       
        /// <returns>List of complaints</returns>

        public IList<Complaint> GetAll(int campaignId, string sd, string ed, string phone, string rescomplaint)
        {
            var sql = @"
                Select *
				From [dbo].[Complaint]
				where [CreatedOn] between @db and @ed
                and [CampaignID] = @CampaignId";

            if(!String.IsNullOrEmpty(phone))
            {
                sql += " and PhoneCustomer like '%" + phone + "%'";
            }

            if (!String.IsNullOrEmpty(rescomplaint))
            {
                sql += " and ComplaintResult = '" + rescomplaint + "'";
            }

            return Connection.Query<Complaint>(sql,
                param: new { CampaignId = campaignId, db = sd, ed = ed },
                transaction: Transaction
               ).ToList();
        }

        public Complaint GetById(int complaintId, int campaignId)
        {
            var sql = @"
                Select *
				From [dbo].[Complaint]
				where [ComplaintId] = @ComplaintId and [CampaignID] = @CampaignID";

            return Connection.Query<Complaint>(sql,
                param: new { ComplaintId = complaintId, CampaignID = campaignId },
                transaction: Transaction
               ).FirstOrDefault();
        }

        public void Add(Complaint item)
        {
            if (item == null)
                throw new ArgumentNullException("entity");

            var sql = @"
                INSERT INTO dbo.Complaint([CampaignID], [ComplaintDescription], [CreatedBy], [CreatedOn], [ComplaintTheme], [ComplaintDateTime], [PhoneCustomer], [ComplaintComment], [ComplaintInformation], [TypeCustomer])
                            VALUES   (@CampaignID,  @ComplaintDescription,  @CreatedBy,  @CreatedOn,  @ComplaintTheme,  @ComplaintDateTime,  @PhoneCustomer,  @ComplaintComment,  @ComplaintInformation, @TypeCustomer);";

            Connection.Execute(
                sql,
                param: new {
                    CampaignID = item.CampaignID,
                    ComplaintDescription = item.ComplaintDescription,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ComplaintTheme = item.ComplaintTheme,
                    ComplaintDateTime = item.ComplaintDateTime,
                    PhoneCustomer = item.PhoneCustomer,
                    ComplaintComment = item.ComplaintComment,
                    ComplaintInformation = item.ComplaintInformation,
                    TypeCustomer = item.TypeCustomer
                },
                transaction: Transaction
            );
        }

        /// <summary>
        /// Update Complaint
        /// </summary>
        /// <param name="Complaint">Complaint to update</param>
        /// <returns>True on success</returns>
        public void Update(Complaint item)
        {
            if (item == null)
                throw new ArgumentNullException("entity");

            var sql = @"
                UPDATE dbo.Complaint SET [RecordCallId] = @RecordCallId, [Operator] = @Operator, [ComplaintResult] = @ComplaintResult, [Responsible] = @Responsible, [Comment] = @Comment, [UpdatedOn] = @UpdatedOn, [UpdatedBy] = @UpdatedBy 
                    WHERE ComplaintId = @ComplaintId and CampaignID = @CampaignID";

            Connection.Execute(
                sql,
                param: new {
                    ComplaintId = item.ComplaintId,
                    CampaignID = item.CampaignID,
                    RecordCallId = item.RecordCallId,
                    Operator = item.Operator,
                    ComplaintResult = item.ComplaintResult,
                    Responsible = item.Responsible,
                    Comment = item.Comment,
                    UpdatedOn = item.UpdatedOn,
                    UpdatedBy = item.UpdatedBy
                },
                transaction: Transaction
            );
        }

        /// <summary>
        /// Delete item
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns>True on success</returns>
        public void Remove(int complaintId, int campaignID)
        {
            Connection.Execute(
                "DELETE FROM [dbo].[Complaint] WHERE [ComplaintId] = @ComplaintId and [CampaignID] = @CampaignID",
                param: new { ComplaintId = complaintId, CampaignID = campaignID },
                transaction: Transaction
            );
        }
    }
}
