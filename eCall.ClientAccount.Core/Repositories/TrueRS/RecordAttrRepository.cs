﻿using Dapper;
using eCall.ClientAccount.Core.Entities.TrueRS;
using eCall.ClientAccount.Core.Interfaces.TrueRS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Repositories.TrueRS
{
    internal class RecordAttrRepository : RepositoryBase, IRecordAttr
    {
        public RecordAttrRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<SkillGroupRecord> AllSGRecordForCustomer(int userId, string type)
        {
            var sql = @"
                        Select csg.SkillsetID, csg.EnterpriseName
From dbo.Campaign_SkillGroup csg
inner join dbo.CampaignType gt on csg.CampaignTypeId = gt.Id
inner join dbo.Campaign c on csg.CampaignID = c.CampaignID
inner join dbo.Groups g on c.CampaignID = g.CampaignID
inner join dbo.CustomerGroup cg on g.Id = cg.GroupsId 
WHERE cg.CustomerId = @CustomerId and gt.Name = @GroupType";


            return Connection.Query<SkillGroupRecord>(sql,
                param: new { GroupType = type, CustomerId = userId },
                transaction: Transaction
               ).ToList();
        }

        public IList<AgentRecord> AllAgentBySG(string listsg)
        {
            var sql = @"
Select distinct o.PeripheralNumber as SkillTargetID, o.LastName + ' ' + o.FirstName as EnterpriseName
From dbo.Operators o
inner join dbo.Operators_SkillGroup_RS osgrs on o.PeripheralNumber = osgrs.PeripheralNumber
where osgrs.SkillsetID in (" + listsg + ")";


            return Connection.Query<AgentRecord>(sql,
                //param: new { ListSG = listsg },
                transaction: Transaction,
                commandTimeout: 620
               ).ToList();
        }

        public EndCallResult GetEndCallResult(string ANI, string StartDate)
        {
            var sql = @"
Select top 1 cdr.OrigCauseValue, cdr.DestCauseValue, cdr.is_transfer
from Vodafone_report.dbo.Vodafone_CDR2 cdr  
where cdr.Phone = @ANI
	and cdr.[EndCall] between CONVERT(varchar(19), Dateadd(SECOND, -60, '" + StartDate + @"'), 120) and CONVERT(varchar(19), Dateadd(SECOND, 60, '" + StartDate + @"'))";

            return Connection.Query<EndCallResult>(sql,
                param: new { ANI = ANI },
                transaction: Transaction,
                commandTimeout: 620
               ).FirstOrDefault();
        }
    }
}
