﻿using Dapper;
using eCall.ClientAccount.Core.Entities.Catalog;
using eCall.ClientAccount.Core.Interfaces.Catalog;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace eCall.ClientAccount.Core.Repositories.Catalog
{
    internal class CategoryRepository : RepositoryBase, ICategory
    {
        public CategoryRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<Category> GetAll(int customerId)
        {
            var sql = @"
                Select c.*
				From [dbo].[CustomerGroup] cg
				inner join [dbo].[Group_Category] gc on cg.GroupsId = gc.[GroupsId]
				inner join [dbo].[Category] c on gc.[CategoryId] = c.Id
				where cg.CustomerId = @CustomerId AND c.Active = 1"  ;


            return Connection.Query<Category>(sql,
                param: new { CustomerId = customerId },
                transaction: Transaction
               ).ToList();
        }

        public IList<ReportCategory> GetReportCategory(int customerId)
        {
            var sql = @"
                Select cg.Id, cg.Name, cg.[Description], cg.CategoryId, cg.[Order], cg.PictureUrl
				From [dbo].[Reports] cg
				inner join [dbo].[Group_Reports] gc on cg.Id = gc.[ReportsId]
				inner join [dbo].[CustomerGroup] c on gc.[GroupsId] = c.GroupsId
				where c.CustomerId = @CustomerId AND cg.Active = 1" ;


            return Connection.Query<ReportCategory>(sql,
                param: new { CustomerId = customerId },
                transaction: Transaction
               ).ToList();
        }
    }
}
