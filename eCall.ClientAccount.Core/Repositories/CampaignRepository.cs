﻿using Dapper;
using eCall.ClientAccount.Core.Interfaces;
using System.Data;
using System.Linq;

namespace eCall.ClientAccount.Core.Repositories
{
    internal class CampaignRepository : RepositoryBase, ICampaign
    {
        public CampaignRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public int GetCampaignByCustomerId(int customerId)
        {
            var sql = @"
                        Select top 1 g.CampaignID
                        from [dbo].[Groups] g
                        inner join [dbo].[CustomerGroup] cg on g.Id = cg.GroupsId
                        where cg.CustomerId = @CustomerId";


            return Connection.Query<int>(sql,
                param: new { CustomerId = customerId },
                transaction: Transaction
               ).FirstOrDefault();
        }
        
    }
}
