﻿using Dapper;
using eCall.ClientAccount.Core.Interfaces.Security;
using System.Data;
using System.Linq;

namespace eCall.ClientAccount.Core.Repositories.Security
{
    internal class AccessRepository : RepositoryBase, IAccess
    {
        public AccessRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public int AccessCategoryUser(int customerId, string url)
        {
            var sql = @"
               SELECT top 1 [Id]      
  FROM [ClientAccount].[dbo].[Category] c
  inner join [ClientAccount].[dbo].[Group_Category] gc on  gc.CategoryId = c.Id 
  inner join [ClientAccount].[dbo].[CustomerGroup] cg on gc.GroupsId = cg.GroupsId
  where [Url] = @Url and cg.CustomerId = @CustomerId";


            return Connection.Query<int>(sql,
                param: new { CustomerId = customerId, Url = url },
                transaction: Transaction
               ).FirstOrDefault();
        }

        public int AccessReportUser(int customerId, int reportId)
        {
            var sql = @"
               SELECT [Id]      
  FROM [ClientAccount].[dbo].[Reports] r
  inner join [ClientAccount].[dbo].[Group_Reports] gc on  gc.ReportsId = r.Id 
  inner join [ClientAccount].[dbo].[CustomerGroup] cg on gc.GroupsId = cg.GroupsId
  where r.Id = @ReportId and cg.CustomerId = @CustomerId";


            return Connection.Query<int>(sql,
                param: new { CustomerId = customerId, ReportId = reportId },
                transaction: Transaction
               ).FirstOrDefault();
        }
    }
}
