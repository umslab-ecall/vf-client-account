﻿using Dapper;
using eCall.ClientAccount.Core.Entities.IPCC;
using eCall.ClientAccount.Core.Interfaces.IPCC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Repositories.IPCC
{
    internal class CallTypeRepository : RepositoryBase, ICallType
    {
        public CallTypeRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<CallTypeFilter> GetAllCustomerCTFilterType(int customerId, string type)
        {
            var sql = @"
                Select distinct cct.CallTypeID, cct.EnterpriseName
From dbo.Campaign_CallType cct
inner join dbo.CampaignType gt on cct.CampaignTypeId = gt.Id
inner join dbo.Campaign c on cct.CampaignID = c.CampaignID
inner join dbo.Groups g on c.CampaignID = g.CampaignID
inner join dbo.CustomerGroup cg on g.Id = cg.GroupsId 
WHERE cg.CustomerId = @CustomerId and gt.Name = @CampaignType";


            return Connection.Query<CallTypeFilter>(sql,
                param: new { CustomerId = customerId, CampaignType = type },
                transaction: Transaction
               ).ToList();
        }
    }
}
