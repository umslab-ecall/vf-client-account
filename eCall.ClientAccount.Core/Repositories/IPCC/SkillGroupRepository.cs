﻿using Dapper;
using eCall.ClientAccount.Core.Entities.IPCC;
using eCall.ClientAccount.Core.Interfaces.IPCC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Repositories.IPCC
{
    internal class SkillGroupRepository : RepositoryBase, ISkillGroup
    {
        public SkillGroupRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<SkillGroupFilter> GetAllCustomerSGFilter(int customerId)
        {
            var sql = @"
                Select distinct csg.SkillTargetID, csg.EnterpriseName
From dbo.Campaign_SkillGroup csg
inner join dbo.CampaignType gt on csg.CampaignTypeId = gt.Id
inner join dbo.Campaign c on csg.CampaignID = c.CampaignID
inner join dbo.Groups g on c.CampaignID = g.CampaignID
inner join dbo.CustomerGroup cg on g.Id = cg.GroupsId
WHERE cg.CustomerId = @CustomerId";


            return Connection.Query<SkillGroupFilter>(sql,
                param: new { CustomerId = customerId },
                transaction: Transaction
               ).ToList();
        }

        public IList<SkillGroupFilter> GetAllCustomerSGFilter(int customerId, string type)
        {
            var sql = @"
                Select distinct csg.SkillTargetID, csg.EnterpriseName
From dbo.Campaign_SkillGroup csg
inner join dbo.CampaignType gt on csg.CampaignTypeId = gt.Id
inner join dbo.Campaign c on csg.CampaignID = c.CampaignID
inner join dbo.Groups g on c.CampaignID = g.CampaignID
inner join dbo.CustomerGroup cg on g.Id = cg.GroupsId
WHERE cg.CustomerId = @CustomerId and gt.Name = @CampaignType";


            return Connection.Query<SkillGroupFilter>(sql,
                param: new { CustomerId = customerId, CampaignType = type },
                transaction: Transaction
               ).ToList();
        }
    }
}
