﻿using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Core.Interfaces.Catalog;
using eCall.ClientAccount.Core.Interfaces.Complaints;
using eCall.ClientAccount.Core.Interfaces.IPCC;
using eCall.ClientAccount.Core.Interfaces.Reports;
using eCall.ClientAccount.Core.Interfaces.Security;
using eCall.ClientAccount.Core.Interfaces.TrueRS;
using eCall.ClientAccount.Core.Repositories.Catalog;
using eCall.ClientAccount.Core.Repositories.Complaints;
using eCall.ClientAccount.Core.Repositories.IPCC;
using eCall.ClientAccount.Core.Repositories.Reports;
using eCall.ClientAccount.Core.Repositories.Security;
using eCall.ClientAccount.Core.Repositories.TrueRS;
using System;
using System.Data;
using System.Data.SqlClient;

namespace eCall.ClientAccount.Core.Repositories
{
    public class UnitOfWorkCA : IUnitOfWorkCA
    {
        #region Fields

        private IDbConnection _connection = null;
        private IDbTransaction _transaction = null;
        private ICustomerAttribute _customerAttributeRepository;
        private IRecordAttr _recordAttrRepository;
        private ICategory _categoryRepository;
        private IReport _reportRepository;
        private IAccess _accessRepository;
        private ISkillGroup _skillGroupRepository;
        private ICallType _callTypeRepository;
        private IComplaint _complaintRepository;
        private ICampaign _campaignRepository;
        private bool _disposed = false;

        #endregion

        #region Constructor

        public UnitOfWorkCA(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            if (_connection.State == ConnectionState.Closed)
                _connection.Open();

            _transaction = _connection.BeginTransaction();
        }

        #endregion

        #region Methods

        public ICustomerAttribute CustomerAttributes
        {
            get { return _customerAttributeRepository ?? (_customerAttributeRepository = new CustomerAttributeRepository(_transaction)); }
        }

        public IRecordAttr RecordAttrs
        {
            get { return _recordAttrRepository ?? (_recordAttrRepository = new RecordAttrRepository(_transaction)); }
        }

        public ICategory Categories
        {
            get { return _categoryRepository ?? (_categoryRepository = new CategoryRepository(_transaction)); }
        }

        public IReport Report
        {
            get { return _reportRepository ?? (_reportRepository = new ReportRepository(_transaction)); }
        }
        public IAccess Access
        {
            get { return _accessRepository ?? (_accessRepository = new AccessRepository(_transaction)); }
        }

        public ISkillGroup SkillGroup
        {
            get { return _skillGroupRepository ?? (_skillGroupRepository = new SkillGroupRepository(_transaction)); }
        }

        public ICallType CallType
        {
            get { return _callTypeRepository ?? (_callTypeRepository = new CallTypeRepository(_transaction)); }
        }

        public IComplaint Complaint
        {
            get { return _complaintRepository ?? (_complaintRepository = new ComplaintRepository(_transaction)); }
        }

        public ICampaign Campaign
        {
            get { return _campaignRepository ?? (_campaignRepository = new CampaignRepository(_transaction)); }
        }

        #endregion

        #region Commit

        public void Commit()
        {
            try
            {
                _transaction.Commit();
                
            }
            catch
            {
                _transaction.Rollback();
                
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();                           
                resetRepositories();
                SqlConnection.ClearAllPools();
            }
        }

        #endregion

        #region ResetRepositories

        private void resetRepositories()
        {
            _customerAttributeRepository = null;
            _recordAttrRepository = null;
            _categoryRepository = null;
            _reportRepository = null;
            _accessRepository = null;
            _skillGroupRepository = null;
            _callTypeRepository = null;
            _complaintRepository = null;
            _campaignRepository = null;
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {                        
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Close();
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                this._disposed = true;
            }
        }

        ~UnitOfWorkCA()
        {
            Dispose(false);
        }

        #endregion
    }
}
