﻿using eCall.ClientAccount.Core.Entities.IPCC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Interfaces.IPCC
{
    public interface ISkillGroup
    {
        IList<SkillGroupFilter> GetAllCustomerSGFilter(int customerId);
        IList<SkillGroupFilter> GetAllCustomerSGFilter(int customerId, string type);
    }
}
