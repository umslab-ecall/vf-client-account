﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Interfaces
{
    public interface ICampaign
    {
        int GetCampaignByCustomerId(int customerId);
    }
}
