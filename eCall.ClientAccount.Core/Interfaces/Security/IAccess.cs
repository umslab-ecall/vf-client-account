﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Interfaces.Security
{
    public interface IAccess
    {
        int AccessCategoryUser(int customerId, string url);
        int AccessReportUser(int customerId, int reportId);
    }
}
