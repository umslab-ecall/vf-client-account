﻿using eCall.ClientAccount.Core.Entities.TrueRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Interfaces.TrueRS
{
    public interface IRecordAttr
    {
        IList<SkillGroupRecord> AllSGRecordForCustomer(int userId, string type);
        IList<AgentRecord> AllAgentBySG(string listsg);
        EndCallResult GetEndCallResult(string ANI, string StartDate);
    }
}
