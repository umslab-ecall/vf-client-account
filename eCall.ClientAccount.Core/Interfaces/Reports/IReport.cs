﻿using eCall.ClientAccount.Core.Entities.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Interfaces.Reports
{
    public interface IReport
    {
        List<Dictionary<string, object>> ExecutSP(string storedProcedure, string skillgroupid, string StartDate, string EndDate);
        List<StatisticsSearch> StatisticsInb(string sd, string ed, string CallTypeID);
        Report GetReportById(int reportId);
        string SkillGroupToCallType(string skillgroupid);
        string GetCompaingNameByCustomer(int customerid);
        List<StatisticsOutStatus> StatisticsOutStatus(string sd, string ed);
        List<StatisticsOutStatusCall> StatisticsOutStatusCall(string sd, string ed);
        List<StatisticsOutStatusNotCall> StatisticsOutStatusNotCall(string sd, string ed);
    }
}
