﻿using eCall.ClientAccount.Core.Entities.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Interfaces.Catalog
{
    public interface ICategory
    {
        IList<Category> GetAll(int customerId);
        IList<ReportCategory> GetReportCategory(int customerId);
    }
}
