﻿using eCall.ClientAccount.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Interfaces
{
    public interface ICustomerAttribute
    {
        IList<CustomerSG> AllSGForCustomer(int userId, string type);
    }
}
