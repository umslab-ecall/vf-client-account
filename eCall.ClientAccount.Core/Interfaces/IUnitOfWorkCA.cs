﻿using eCall.ClientAccount.Core.Interfaces.Catalog;
using eCall.ClientAccount.Core.Interfaces.Complaints;
using eCall.ClientAccount.Core.Interfaces.IPCC;
using eCall.ClientAccount.Core.Interfaces.Reports;
using eCall.ClientAccount.Core.Interfaces.Security;
using eCall.ClientAccount.Core.Interfaces.TrueRS;
using System;

namespace eCall.ClientAccount.Core.Interfaces
{
    public interface IUnitOfWorkCA : IDisposable
    {
        IRecordAttr RecordAttrs { get; }
        ICustomerAttribute CustomerAttributes { get; }
        ICategory Categories { get; }
        IReport Report { get; }
        IAccess Access { get; }
        ISkillGroup SkillGroup { get; }
        ICallType CallType { get; }
        IComplaint Complaint { get; }
        ICampaign Campaign { get; }
        void Commit();
    }
}
