﻿using eCall.ClientAccount.Core.Entities.Complaints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Interfaces.Complaints
{
    public interface IComplaint
    {
        IList<Complaint> GetAll(int campaignId, string sd, string ed, string phone, string rescomplaint);
        Complaint GetById(int complaintId, int campaignId);
        void Add(Complaint item);
        /// <summary>
        /// Update Complaint
        /// </summary>
        /// <param name="Complaint">Complaint to update</param>
        /// <returns>True on success</returns>
        void Update(Complaint blog);
        /// <summary>
        /// Delete item
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns>True on success</returns>
        void Remove(int complaintId, int campaignID);
    }
}
