﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Entities.Complaints
{
    public class Complaint
    {
        public int ComplaintId { get; set; }
        public int CampaignID { get; set; }        
        public string ComplaintDescription { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ComplaintTheme { get; set; }
        public DateTime ComplaintDateTime { get; set; }
        public string PhoneCustomer { get; set; }
        public string TypeCustomer { get; set; }
        public string ComplaintComment { get; set; }
        public string ComplaintInformation { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string RecordCallId { get; set; }
        public string Operator { get; set; }
        public string ComplaintResult { get; set; }
        public string Responsible { get; set; }
        public string Comment { get; set; }
    }
}
