﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Entities.IPCC
{
    public class SkillGroupFilter
    {
        public int SkillTargetID { get; set; }
        public string EnterpriseName { get; set; }
    }
}
