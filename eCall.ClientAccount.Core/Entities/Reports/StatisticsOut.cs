﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Entities.Reports
{
    public class StatisticsOutStatus
    {
        public string CallStatus { get; set; }
        public string CountRec { get; set; }
        public string RoundRec { get; set; }
    }

    public class StatisticsOutStatusCall
    {
        public string Result { get; set; }
        public string CountRec { get; set; }
        public string RoundRec { get; set; }
    }

    public class StatisticsOutStatusNotCall
    {
        public string Result { get; set; }
        public string CountRec { get; set; }
        public string RoundRec { get; set; }
    }
}
