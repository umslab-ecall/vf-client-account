﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Entities.TrueRS
{
    public class EndCallResult
    {
        public int OrigCauseValue { get; set; }
        public int DestCauseValue { get; set; }
        public int is_transfer { get; set; }
    }
}
