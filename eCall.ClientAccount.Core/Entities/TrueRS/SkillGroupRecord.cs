﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Core.Entities.TrueRS
{
    public class SkillGroupRecord
    {
        public int SkillsetID { get; set; }
        public string EnterpriseName { get; set; }
    }
}
