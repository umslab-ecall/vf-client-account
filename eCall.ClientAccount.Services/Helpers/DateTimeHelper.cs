﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Helpers
{
    public static class DateTimeHelper
    {
        public static string DateToStringSQLFormat(this string date)
        {
            DateTime dd;
            if (String.IsNullOrEmpty(date))
            {
                dd = DateTime.Now;
            } else
            {
                dd = Convert.ToDateTime(date);
            }
            return dd.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
