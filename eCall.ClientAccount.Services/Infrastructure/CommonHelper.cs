﻿namespace eCall.ClientAccount.Services.Infrastructure
{
    public static class CommonHelper
    {
        public static string EnsureNotNull(this string str)
        {
            if (str == null)
                return string.Empty;

            return str;
        }
    }
}
