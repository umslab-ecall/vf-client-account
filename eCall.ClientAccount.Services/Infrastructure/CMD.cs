﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace eCall.ClientAccount.Services.Infrastructure
{
    public class CMD
    {
        public static int elapsedTime;
        public static bool eventHandled;

        public static void process_Exited(object sender, EventArgs e)
        {
            eventHandled = true;
        }
        public static void ConvertGToWav(string from, string to)
        {
            const int SLEEP_AMOUNT = 100;
            elapsedTime = 0;
            eventHandled = false;
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //startInfo.FileName = "cmd.exe";
            startInfo.FileName = @"C:\Programs\ffmpeg\bin\ffmpeg.exe";
            process.EnableRaisingEvents = true;
            process.Exited += new EventHandler(process_Exited);
            //startInfo.Arguments = @"/C copy " + from + " " + to;
            startInfo.Arguments = " -i " + from + " " + to;
            process.StartInfo = startInfo;
            process.Start();
            while (!eventHandled)
            {
                elapsedTime += SLEEP_AMOUNT;
                if (elapsedTime > 30000)
                {
                    break;
                }
                Thread.Sleep(SLEEP_AMOUNT);
            }
        }

        public static double GetAudioVolume(object audioPath)
        {
            const int SLEEP_AMOUNT = 100;
            elapsedTime = 0;
            eventHandled = false;

            double volume = 0;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            var startInfo = new ProcessStartInfo(@"C:\Programs\sox\sox.exe",
                string.Format("\"{0}\" -n stat", audioPath));
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;
            process.EnableRaisingEvents = true;
            process.Exited += new EventHandler(process_Exited);
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

            while (!eventHandled)
            {
                elapsedTime += SLEEP_AMOUNT;
                if (elapsedTime > 30000)
                {
                    break;
                }
                Thread.Sleep(SLEEP_AMOUNT);
            }

            string str;
            using (var outputThread = process.StandardError)
                str = outputThread.ReadToEnd();

            if (string.IsNullOrEmpty(str))
                using (var outputThread = process.StandardOutput)
                    str = outputThread.ReadToEnd();

            try
            {
                string[] lines = str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string lengthLine = lines.First(line => line.Contains("Volume adjustment")).Split(':')[1].Replace(" ", "").Replace(".", ",").ToString();
                volume = Convert.ToDouble(lengthLine);
            }
            catch (Exception ex)
            {
                //Func.Clog.Message(ex);
            }
            elapsedTime = 0;
            eventHandled = false;
            process.Close();
            return volume;
        }

        public static void MixToOgg(string _path, string _name1, string _name2)
        {
            const int SLEEP_AMOUNT = 100;
            elapsedTime = 0;
            eventHandled = false;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //startInfo.FileName = "cmd.exe";
            startInfo.FileName = @"C:\Programs\sox\sox.exe";
            process.EnableRaisingEvents = true;
            process.Exited += new EventHandler(process_Exited);
            startInfo.Arguments = "-m " + _path + @"\" + _name1 + @".ogg " + _path + @"\" + _name2 + @".ogg " + _path + _name1 + "_res_" + @".ogg";
            process.StartInfo = startInfo;
            process.Start();
            while (!eventHandled)
            {
                elapsedTime += SLEEP_AMOUNT;
                if (elapsedTime > 30000)
                {
                    break;
                }
                Thread.Sleep(SLEEP_AMOUNT);
            }

            elapsedTime = 0;
            eventHandled = false;
            process.Close();
        }

        public static void MixToWav(string _path, string _name1, string _name2)
        {
            const int SLEEP_AMOUNT = 100;
            elapsedTime = 0;
            eventHandled = false;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //startInfo.FileName = "cmd.exe";
            startInfo.FileName = @"C:\Programs\sox\sox.exe";
            process.EnableRaisingEvents = true;
            process.Exited += new EventHandler(process_Exited);
            startInfo.Arguments = "-m " + _path + @"\" + _name1 + @".wav " + _path + @"\" + _name2 + @".wav " + _path + @"\wav\" + _name1 + @".wav";
            process.StartInfo = startInfo;
            process.Start();
            while (!eventHandled)
            {
                elapsedTime += SLEEP_AMOUNT;
                if (elapsedTime > 30000)
                {
                    break;
                }
                Thread.Sleep(SLEEP_AMOUNT);
            }

            elapsedTime = 0;
            eventHandled = false;
            process.Close();
        }

        public static void ConvertToMp3(string file)
        {
            const int SLEEP_AMOUNT = 100;
            elapsedTime = 0;
            eventHandled = false;

            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.EnableRaisingEvents = false;
            proc.StartInfo.FileName = @"C:\Programs\ffmpeg\bin\ffmpeg.exe";
            proc.StartInfo.Arguments = "-i " + file + " -vn -ar 44100 -ac 2 -ab 192k -f mp3 " + file.Replace(".wav", ".mp3");
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = false;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.EnableRaisingEvents = true;
            proc.Exited += new EventHandler(process_Exited);
            proc.Start();
            proc.WaitForExit();

            while (!eventHandled)
            {
                elapsedTime += SLEEP_AMOUNT;
                if (elapsedTime > 30000)
                {
                    break;
                }
                Thread.Sleep(SLEEP_AMOUNT);
            }
            elapsedTime = 0;
            eventHandled = false;
            proc.Close();
        }

        public static void CopyRecord(double volume, string from, string to, int padBegin, int padEnd)
        {
            if (padBegin < 0) padBegin = 0;
            if (padEnd < 0) padEnd = 0;
            const int SLEEP_AMOUNT = 100;
            elapsedTime = 0;
            eventHandled = false;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //startInfo.FileName = "cmd.exe";
            startInfo.FileName = @"C:\Programs\sox\sox.exe";
            process.EnableRaisingEvents = true;
            process.Exited += new EventHandler(process_Exited);
            //startInfo.Arguments = @"/C copy " + from + " " + to;
            startInfo.Arguments = " -v " + volume.ToString().Replace(",", ".") + " " + from + " " + to + " pad " + padBegin + " " + padEnd;
            process.StartInfo = startInfo;
            process.Start();
            while (!eventHandled)
            {
                elapsedTime += SLEEP_AMOUNT;
                if (elapsedTime > 30000)
                {
                    break;
                }
                Thread.Sleep(SLEEP_AMOUNT);
            }

            elapsedTime = 0;
            eventHandled = false;
            process.Close();
        }
    }
}
