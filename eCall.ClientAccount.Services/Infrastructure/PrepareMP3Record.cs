﻿using eCall.ClientAccount.Services.DTO.TrueRS;
using eCall.ClientAccount.Services.Services.TrueRS;
using NAudio.FileFormats.Mp3;
using NAudio.Lame;
using NAudio.Wave;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Infrastructure
{
    public static class PrepareMP3Record
    {
        static Logger logger = LogManager.GetCurrentClassLogger();

        public static IList<RecordDTO> PrepareMP3RecordForPlay(IList<RecordDTO> RecordList, int id, string serverpath, string browser)
        {
            CheckAddBinPath();
            if (!Directory.Exists(serverpath))
            {
                Directory.CreateDirectory(serverpath);
            }

            List<string> listRecordPre = new List<string>();

            long _hold = Convert.ToInt64(RecordList.ElementAt(0).Duration);
            
            string recordFolder = serverpath + @"\" + id;
            string temp = recordFolder + @"\temp";
            string recordFormat = "";
            bool _ifExist = false;
            string _to1 = null;

            if (browser == "InternetExplorer")
            {
                recordFormat = "wav";
                
                if (!Directory.Exists(recordFolder + @"\" + recordFormat))// Слушали запись ранее или нет?
                {
                    Directory.CreateDirectory(recordFolder + @"\" + recordFormat);// Создание папки для .wav файлов
                }
                else { _ifExist = true; }
                recordFolder += @"\";
            }
            else
            {
                recordFormat = "wav";

                if (Directory.Exists(recordFolder + @"\" + recordFormat) && Directory.GetFiles(recordFolder + @"\" + recordFormat).Count() < 1)// Слушали запись ранее или нет?
                    Directory.Delete(recordFolder + @"\" + recordFormat);

                if (!Directory.Exists(recordFolder + @"\" + recordFormat))// Слушали запись ранее или нет?
                {
                    Directory.CreateDirectory(recordFolder + @"\" + recordFormat);// CMD.SozdatPapku(_papka + @"\" + _format);// Создание папки для .ogg файлов
                    Directory.CreateDirectory(temp);
                }
                else { _ifExist = true; }
                recordFolder += @"\wav\";
            }

            for (int i = 1; i < RecordList.Count;)
            {
                try
                {
                    // Вычисление холда. От общей продолжительности отнимаем продолжительность первой части
                    _hold -= Convert.ToInt64(RecordList.ElementAt(i).Duration);                    

                    long _begin1 = Convert.ToDateTime(RecordList.ElementAt(i).StartTime).Ticks;// Начало первого файла в тиках
                    long _begin2 = Convert.ToDateTime(RecordList.ElementAt(i + 1).StartTime).Ticks;// Начало второго файла в тиках
                    double _dur1 = 1;                    

                    //NAudio.Wave       
                    string _from1 = RecordList.ElementAt(i).FilePath;// Откуда брать первый файл
                    string _from2 = RecordList.ElementAt(i + 1).FilePath;// Откуда брать второй файл

                    //Временый файли
                    string tempfolder1 = temp + @"\" + RecordList.ElementAt(i).vrID + ".wav";
                    string tempfolder2 = temp + @"\" + RecordList.ElementAt(i + 1).vrID + ".wav";

                    _to1 = recordFolder + RecordList.ElementAt(i).vrID + "." + recordFormat;// Куда сохранить и в каком формате(первый файл)
                    string _to2 = recordFolder + RecordList.ElementAt(i + 1).vrID + "." + recordFormat;// Куда сохранить и в каком формате(второй файл)

                    var impersonationContext = new WrappedImpersonationContext("IPCC", "PKovtun", "oiMuZV2r");
                    impersonationContext.Enter();
                    if (File.Exists(_from1) && File.Exists(_from2))
                    {
                        File.Copy(_from1, tempfolder1);
                        File.Copy(_from2, tempfolder2);
                    }
                    else
                    {
                        return null;                        
                    }
                    impersonationContext.Leave();

                    if(RecordList.Count <= 3)
                    {
                        Task task = Task.Factory.StartNew(() =>
                        {
                            using (WaveMixerStream32 mixStream = new WaveMixerStream32())
                            {
                                mixStream.AutoStop = true;
                                AudioFileReader afr1 = new AudioFileReader(tempfolder1);
                                AudioFileReader afr2 = new AudioFileReader(tempfolder2);

                                mixStream.AddInputStream(afr1);
                                mixStream.AddInputStream(afr2);

                                WaveFileWriter.CreateWaveFile(recordFolder + id + "." + recordFormat, mixStream);
                                mixStream.Close();
                            }
                        });
                        task.Wait();

                        DeleteTempFile(tempfolder1);
                        DeleteTempFile(tempfolder2);

                        if (Directory.Exists(temp))
                        {
                            Directory.Delete(temp);
                        }
                    }
                    if (RecordList.Count > 3)
                    {
                        Task task11 = Task.Factory.StartNew(() =>
                        {
                            using (WaveMixerStream32 mixStream = new WaveMixerStream32())
                            {
                                mixStream.AutoStop = true;
                                AudioFileReader afr1 = new AudioFileReader(tempfolder1);
                                AudioFileReader afr2 = new AudioFileReader(tempfolder2);

                                mixStream.AddInputStream(afr1);
                                mixStream.AddInputStream(afr2);

                                WaveFileWriter.CreateWaveFile(_to1, mixStream);
                                mixStream.Close();
                            }
                        });
                        task11.Wait();

                        string mp3record = recordFolder + RecordList.ElementAt(i).vrID + ".mp3";

                        Task task1 = Task.Factory.StartNew(() =>
                        {
                            WaveToMP3(_to1, mp3record);
                        });
                        task1.Wait();

                        //Task task2 = Task.Factory.StartNew(() =>
                        //{
                        //    MP3ToWave(mp3record, mp3record + "new.wav");                           
                        //});
                        //task2.Wait();
                        //logger.Info("MP3ToWave");
                        DeleteTempFile(_to1);
                        DeleteTempFile(tempfolder1);
                        DeleteTempFile(tempfolder2);

                        listRecordPre.Add(mp3record);

                        if ((i + 2) < RecordList.Count)
                        {
                            DateTime dtstart = RecordList.ElementAt(i).StartTime.AddSeconds(Convert.ToInt64(RecordList.ElementAt(i).Duration));
                            DateTime dt = dtstart.AddSeconds(Convert.ToInt64(RecordList.ElementAt(i + 2).Duration));
                            TimeSpan span = RecordList.ElementAt(i + 2).StartTime - dtstart;


                            Task task2 = Task.Factory.StartNew(() =>
                            {
                                WaveToMP3(serverpath + @"\CompaingHold\Vodafone.wav", temp + @"\VodafoneHold.mp3");
                            });
                            task2.Wait();


                            //Thread.Sleep(1000);


                            Task task3 = Task.Factory.StartNew(() =>
                            {
                                recordTrim((int)span.TotalSeconds, temp + @"\VodafoneHold.mp3", recordFolder + @"hold" + i);
                            });
                            task3.Wait();

                            //Thread.Sleep(1000);

                            listRecordPre.Add(recordFolder + @"hold" + i + ".mp3");

                            //DeleteTempFile(temp + @"\VodafoneHold.mp3");

                            RecordList.ElementAt(i).Hold = (int)(Convert.ToDateTime(RecordList.ElementAt(i + 2).StartTime).Ticks / 10000000 - _begin1 / 10000000 - _dur1);
                        }
                    }
                }
                catch (Exception exc)
                {
                    logger.Error("MP3ToWave" + exc.Message);
                    //Func.Clog.Message(exc);
                    //Console.WriteLine("{0} Exception caught.", exc);
                } //(InvalidCastException e)
                i += 2;
            }
            
            RecordList.ElementAt(0).Hold = (int)_hold;

            //if (Directory.Exists(temp))
            //{
            //    Directory.Delete(temp);
            //}

            //Swap(listRecordPre, 1, 2);
            if (listRecordPre.Count > 1)
            {
                Combine(listRecordPre, File.Create(recordFolder + id + @".mp3"));
                MP3ToWave(recordFolder + id + @".mp3", recordFolder + id + @".wav");
                RecordList.ElementAt(0).FilePath = listRecordPre.First();
            }
            else
            {                
                RecordList.ElementAt(0).FilePath = recordFolder + id + "." + recordFormat;
            }
            //WavFileUtils.Concatenate(recordFolder + id + @".wav", listRecordPreWAV);
            
            //DeleteTempFile(recordFolder + id + @".mp3");
            
            return RecordList;
        }

        private static void DeleteTempFile(string FilePath)
        {
            if (File.Exists(FilePath))
            {
                File.Delete(FilePath);
            }
        }

        public static void Swap<T>(this List<T> list, int index1, int index2)
        {
            T temp = list[index1];
            list[index1] = list[index2];
            list[index2] = temp;
        }

        public static void recordTrim(int hold, string mp3pathIn, string mp3pathOut)
        {
            CheckAddBinPath();            
            int splitLength = hold; // seconds 
            int countHold = 0;
            int secsOffset = 0;
            List<string> listHold = new List<string>();

            //Stream writer = File.Create(mp3pathOut + "1.mp3");
            //Thread.Sleep(2000);
            using (var reader = new Mp3FileReader(mp3pathIn))
            {
                FileStream writer = null;
                Action createWriter = new Action(() =>
                {
                    writer = File.Create(mp3pathOut + "1.mp3");
                });

                if ((int)reader.TotalTime.TotalSeconds < splitLength)
                {
                    countHold = splitLength / (int)reader.TotalTime.TotalSeconds;
                    splitLength = splitLength % (int)reader.TotalTime.TotalSeconds;
                    //splitLength = splitLength - (count * (int)reader.TotalTime.TotalSeconds);
                }

                Mp3Frame frame;
                while ((frame = reader.ReadNextFrame()) != null)
                {
                    if (writer == null) createWriter();

                    if ((int)reader.CurrentTime.TotalSeconds - secsOffset >= splitLength)
                    {
                        // time for a new file 
                        writer.Dispose();
                        writer.Close();
                        if (countHold != 0)
                        {
                            for (int i = 0; i < countHold; i++)
                            {
                                listHold.Add(mp3pathIn);
                            }


                            //writer.Close();
                            //DeleteTempFile(mp3pathOut + "1.mp3");
                        }
                        listHold.Add(mp3pathOut + "1.mp3");

                        Combine(listHold, File.Create(mp3pathOut + ".mp3"));

                        //DeleteTempFile(mp3pathOut + "1.mp3");

                        return;
                    }

                    writer.WriteAsync(frame.RawData, 0, frame.RawData.Length);
                }

                if (writer != null) writer.Dispose();
            }
        }

        // Convert WAV to MP3 using libmp3lame library
        public static void WaveToMP3(string waveFileName, string mp3FileName, int bitRate = 128)
        {
            try
            {
                CheckAddBinPath();
                using (var reader = new AudioFileReader(waveFileName))
                {
                    using (var writer = new LameMP3FileWriter(mp3FileName, reader.WaveFormat, bitRate))
                    {
                        reader.CopyTo(writer);
                    }
                }
            }
            catch (Exception exc)
            {
                logger.Error("MP3ToWave" + exc.Message);
            }
            
        }

        // Convert MP3 file to WAV using NAudio classes only
        public static void MP3ToWave(string mp3FileName, string waveFileName)
        {
            try
            {
                CheckAddBinPath();
                using (Stream file = File.OpenRead(mp3FileName))
                using (var reader = new Mp3FileReader(file, wave => new DmoMp3FrameDecompressor(wave)))
                {
                    using (var writer = new WaveFileWriter(waveFileName, reader.WaveFormat))
                    {
                        reader.CopyTo(writer);
                    }
                }
            }
            catch (Exception exc)
            {
                logger.Error("MP3ToWave" + exc.Message);
            }
        }

        public static void CheckAddBinPath()
        {
            // find path to 'bin' folder
            var binPath = Path.Combine(new string[] { AppDomain.CurrentDomain.BaseDirectory, "bin" });
            // get current search path from environment
            var path = Environment.GetEnvironmentVariable("PATH") ?? "";

            // add 'bin' folder to search path if not already present
            if (!path.Split(Path.PathSeparator).Contains(binPath, StringComparer.CurrentCultureIgnoreCase))
            {
                path = string.Join(Path.PathSeparator.ToString(), new string[] { path, binPath });
                Environment.SetEnvironmentVariable("PATH", path);
            }
        }

        private static void Combine(List<string> inputFiles, Stream output)
        {
            try
            {
                CheckAddBinPath();
                Mp3FileReader reader = null;
                foreach (string file in inputFiles)
                {
                    reader = new Mp3FileReader(file);
                    if ((output.Position == 0) && (reader.Id3v2Tag != null))
                    {
                        output.WriteAsync(reader.Id3v2Tag.RawData, 0, reader.Id3v2Tag.RawData.Length);
                    }
                    Mp3Frame frame;
                    while ((frame = reader.ReadNextFrame()) != null)
                    {
                        output.WriteAsync(frame.RawData, 0, frame.RawData.Length);
                    }

                }
                //output.Dispose();
                //reader.Close();
                output.Close();
            }
            catch (Exception exc)
            {
                logger.Error("MP3ToWave" + exc.Message);
            }    
        }
    }
}
