﻿using eCall.ClientAccount.Services.DTO.TrueRS;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Infrastructure
{
    public class PrepareRecord
    {
        public static IList<RecordDTO> PrepareRecordForPlay(IList<RecordDTO> RecordList, int id, string serverpath, string browser)
        {
            if (!Directory.Exists(serverpath))
            {
                Directory.CreateDirectory(serverpath);
            }

            long _hold = Convert.ToInt64(RecordList.ElementAt(0).Duration);
            string recordFolder = serverpath + @"\" + id;
            string temp = recordFolder + @"\temp";
            string recordFormat = "";
            bool _ifExist = false;

            if (browser == "InternetExplorer")
            {
                recordFormat = "wav";

                /*if (Directory.Exists(_papka + @"\" + _format) && Directory.GetFiles(_papka + @"\" + _format).Count() < 1)// Слушали запись ранее или нет?
                    Directory.Delete(_papka + @"\" + _format);*/

                if (!Directory.Exists(recordFolder + @"\" + recordFormat))// Слушали запись ранее или нет?
                {
                    Directory.CreateDirectory(recordFolder + @"\" + recordFormat);// Создание папки для .wav файлов
                }
                else { _ifExist = true; }
                recordFolder += @"\";
            }
            else
            {
                recordFormat = "ogg";

                if (Directory.Exists(recordFolder + @"\" + recordFormat) && Directory.GetFiles(recordFolder + @"\" + recordFormat).Count() < 1)// Слушали запись ранее или нет?
                    Directory.Delete(recordFolder + @"\" + recordFormat);

                if (!Directory.Exists(recordFolder + @"\" + recordFormat))// Слушали запись ранее или нет?
                {
                    Directory.CreateDirectory(recordFolder + @"\" + recordFormat);// CMD.SozdatPapku(_papka + @"\" + _format);// Создание папки для .ogg файлов
                    Directory.CreateDirectory(temp);
                }
                else { _ifExist = true; }
                recordFolder += @"\ogg\";
            }

            for (int i = 1; i < RecordList.Count;)
            {
                try
                {
                    // Вычисление холда. От общей продолжительности отнимаем продолжительность первой части
                    _hold -= Convert.ToInt64(RecordList.ElementAt(i).Duration);

                    long _begin1 = Convert.ToDateTime(RecordList.ElementAt(i).StartTime).Ticks;// Начало первого файла в тиках
                    long _begin2 = Convert.ToDateTime(RecordList.ElementAt(i + 1).StartTime).Ticks;// Начало второго файла в тиках
                    double _dur1 = 1;
                    double _dur2 = 1;
                    
                    double volume1 = 1;
                    double volume2 = 1;
                                       
                    //NAudio.Wave       
                    string _from1 = RecordList.ElementAt(i).FilePath;// Откуда брать первый файл
                    string _from2 = RecordList.ElementAt(i + 1).FilePath;// Откуда брать второй файл

                    //Временый файли
                    string tempfolder1 = temp + @"\" + RecordList.ElementAt(i).vrID + ".wav";
                    string tempfolder2= temp + @"\" + RecordList.ElementAt(i+1).vrID + ".wav";

                    string _to1 = recordFolder + RecordList.ElementAt(i).vrID + "." + recordFormat;// Куда сохранить и в каком формате(первый файл)
                    string _to2 = recordFolder + RecordList.ElementAt(i + 1).vrID + "." + recordFormat;// Куда сохранить и в каком формате(второй файл)

                    var impersonationContext = new WrappedImpersonationContext("IPCC", "PKovtun", "oiMuZV2r");
                    impersonationContext.Enter();
                    File.Copy(_from1, tempfolder1);
                    File.Copy(_from2, tempfolder2);
                    impersonationContext.Leave();

                    if (tempfolder1.Contains(".g729") && tempfolder2.Contains(".g729"))
                    {
                        recordFormat = "g729";
                        string _from1tmp = tempfolder1;
                        string _from2tmp = tempfolder2;
                        tempfolder1 = recordFolder + RecordList.ElementAt(i).vrID + ".wav";
                        tempfolder2 = recordFolder + RecordList.ElementAt(i + 1).vrID + ".wav";
                        CMD.ConvertGToWav(_from1tmp, tempfolder1);
                        CMD.ConvertGToWav(_from2tmp, tempfolder2);
                    }

                    try
                    {
                        Task<double> task1 = Task<double>.Factory.StartNew(() =>
                        {
                            return new Volume(RecordList.ElementAt(i).FilePath, volume1).GetAudioVolume(RecordList.ElementAt(i).FilePath);
                        });

                        task1.Wait();

                        double vol1 = task1.Result;

                        if (vol1 > 2)
                        {
                            volume1 = vol1 - 1;
                        }
                        else
                        {
                            volume1 = 1;
                        }                       

                    }
                    catch (Exception ex)
                    {
                        //Func.Clog.Message(ex);
                    }


                    try
                    {
                        //GetAudioVolume(RecordPartList.ElementAt(i + 1).Path);
                        Task<double> task2 = Task<double>.Factory.StartNew(() =>
                        {
                            return new Volume(RecordList.ElementAt(i + 1).FilePath, volume1).GetAudioVolume(RecordList.ElementAt(i + 1).FilePath);
                        });

                        task2.Wait();

                        double vol2 = task2.Result;

                        if (vol2 > 2)
                        {
                            volume2 = vol2 - 1;
                        }
                        else
                        {
                            volume2 = 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Func.Clog.Message(ex);
                    }



                    /*int _dur1 = Convert.ToInt32(_RecordPartList.ElementAt(i).Duration);// Продолжительность первого файла
                    int _dur2 = Convert.ToInt32(_RecordPartList.ElementAt(i + 1).Duration);// Продолжительность второго файла*/
                    try
                    {
                        if (RecordList.ElementAt(i).FilePath.Contains(".wav"))
                        {
                            //var impersonationContext = new WrappedImpersonationContext("IPCC", "PKovtun", "oiMuZV2r");
                            //impersonationContext.Enter();

                            //start working with another username
                            WaveFileReader wave3 = new WaveFileReader(RecordList.ElementAt(i).FilePath);

                            //finish working with another username
                            //impersonationContext.Leave();

                            _dur1 = wave3.TotalTime.TotalSeconds;
                            //_dur1 = CMD.GetAudioDuration(RecordPartList.ElementAt(i).Path);// Продолжительность первого файла
                        }
                        else
                        {
                            _dur1 = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Func.Clog.Message(ex);
                    }


                    try
                    {
                        if (RecordList.ElementAt(i + 1).FilePath.Contains(".wav"))
                        {
                            //var impersonationContext = new WrappedImpersonationContext("IPCC", "PKovtun", "oiMuZV2r");
                            //impersonationContext.Enter();

                            //start working with another username
                            WaveFileReader wave4 = new WaveFileReader(RecordList.ElementAt(i + 1).FilePath);

                            //finish working with another username
                            //impersonationContext.Leave();
                            _dur2 = wave4.TotalTime.TotalSeconds;
                            //_dur2 = CMD.GetAudioDuration(RecordPartList.ElementAt(i + 1).Path);// Продолжительность второго файла
                        }
                        else
                        {
                            _dur2 = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Func.Clog.Message(ex);
                    }


                    if (i + 2 < RecordList.Count)
                        RecordList.ElementAt(i).Hold = (int)(Convert.ToDateTime(RecordList.ElementAt(i + 2).StartTime).Ticks / 10000000 - _begin1 / 10000000 - _dur1);



                    if (!_ifExist)
                    {
                        //var impersonationContext = new WrappedImpersonationContext("IPCC", "PKovtun", "oiMuZV2r");
                        //impersonationContext.Enter();

                        //start working with another username
                        // Копируем файлы с поправками начала и конца файла(SOX -> pad)
                        Task task3 = Task.Factory.StartNew(() =>
                        {
                            CMD.CopyRecord(Convert.ToDouble(volume1), tempfolder1, _to1, Convert.ToInt32(_dur2 - _dur1), Convert.ToInt32((_begin1 - _begin2) / 10000000));
                            CMD.CopyRecord(Convert.ToDouble(volume2), tempfolder2, _to2, Convert.ToInt32(_dur1 - _dur2), Convert.ToInt32((_begin2 - _begin1) / 10000000));
                            
                        });
                        task3.Wait();

                        //finish working with another username
                        

                        if (browser == "InternetExplorer")
                        {
                            // Микшируем для IE
                            try
                            {
                                Task task5 = Task.Factory.StartNew(() =>
                                {
                                    try
                                    {
                                        CMD.MixToWav(recordFolder, RecordList.ElementAt(i).vrID.ToString(), RecordList.ElementAt(i + 1).vrID.ToString());
                                    }
                                    catch (Exception ex)
                                    {
                                        //Func.Clog.Message(ex);
                                    }
                                });

                                Task task6 = task5.ContinueWith((k) =>
                                {
                                    try
                                    {
                                        CMD.ConvertToMp3(recordFolder + @"\wav\" + RecordList.ElementAt(i).vrID + @".wav");
                                    }
                                    catch (Exception ex)
                                    {
                                        //Func.Clog.Message(ex);
                                    }
                                });

                                task6.Wait();
                            }
                            catch (Exception ex)
                            {
                                //Func.Clog.Message(ex);
                            }
                        }
                        else
                        {
                            try
                            {
                                Task task7 = Task.Factory.StartNew(() =>
                                {
                                    try
                                    {
                                        CMD.MixToOgg(recordFolder, RecordList.ElementAt(i).vrID.ToString(), RecordList.ElementAt(i + 1).vrID.ToString());                                        
                                    }
                                    catch (Exception ex)
                                    {
                                        //Func.Clog.Message(ex);
                                    }

                                });
                                task7.Wait();

                            }
                            catch (Exception ex)
                            {
                                //Func.Clog.Message(ex);
                            }

                        }
                    }
                }
                catch (Exception exc)
                {
                    //Func.Clog.Message(exc);
                    //Console.WriteLine("{0} Exception caught.", exc);
                } //(InvalidCastException e)
                i += 2;
                
            }

            // Удаление временных файлов 
            //CMD.UdalitFiles(recordFolder + "*.wav");

            RecordList.ElementAt(0).Hold = (int)_hold;

            return RecordList;
        }
    }

    public class Volume
    {
        public string path;
        public double vol;
        public Volume(string _path, double _vol)
        {
            path = _path;
            vol = _vol;
        }

        public double GetAudioVolume(string path)
        {
            try
            {
                vol = CMD.GetAudioVolume(path);
            }
            catch (Exception ex)
            {
                //Func.Clog.Message(ex);
            }
            return vol;
        }
    }
}
