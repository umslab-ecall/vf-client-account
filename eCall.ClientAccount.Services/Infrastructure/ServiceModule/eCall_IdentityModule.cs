﻿using eCall.Identity.Core.Interfaces;
using eCall.Identity.Core.Repositories;
using Ninject.Modules;

namespace eCall.ClientAccount.Services.Infrastructure.ServiceModule
{
    public class eCall_IdentityModule : NinjectModule
    {
        private string connectionString;
        public eCall_IdentityModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}
