﻿using eCall.TrueRS.Core.Interfaces;
using eCall.TrueRS.Core.Repositories;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Infrastructure.ServiceModule
{
    public class eCall_TrueRSModule : NinjectModule
    {
        private string connectionString;
        public eCall_TrueRSModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWorkTrueRS>().To<UnitOfWorkTrueRS>().WithConstructorArgument(connectionString);
        }
    }
}
