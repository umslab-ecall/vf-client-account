﻿using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Core.Repositories;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Infrastructure.ServiceModule
{
    public class eCall_ClientAccountModule : NinjectModule
    {
        private string connectionString;
        public eCall_ClientAccountModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWorkCA>().To<UnitOfWorkCA>().WithConstructorArgument(connectionString);
        }
    }
}
