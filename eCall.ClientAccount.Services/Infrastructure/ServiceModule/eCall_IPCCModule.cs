﻿using eCall.IPCC.Core.Interfaces;
using eCall.IPCC.Core.Repositories;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Infrastructure.ServiceModule
{
    public class eCall_IPCCModule : NinjectModule
    {
        private string connectionString;
        public eCall_IPCCModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWorkIPCC>().To<UnitOfWorkIPCC>().WithConstructorArgument(connectionString);
        }
    }
}
