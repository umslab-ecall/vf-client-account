﻿using System.Collections.Generic;

namespace eCall.ClientAccount.Services.DTO.Security
{
    public class RoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<UserDTO> Users { get; set; }
        public RoleDTO()
        {
            Users = new List<UserDTO>();
        }
    }
}
