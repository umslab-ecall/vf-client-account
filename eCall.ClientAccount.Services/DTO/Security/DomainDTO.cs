﻿namespace eCall.ClientAccount.Services.DTO.Security
{
    public class DomainDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
