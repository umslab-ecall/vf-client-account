﻿using System;
using System.Collections.Generic;

namespace eCall.ClientAccount.Services.DTO.Security
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public virtual IList<RoleDTO> Roles { get; set; }
        public UserDTO()
        {
            Roles = new List<RoleDTO>();
        }
    }
}
