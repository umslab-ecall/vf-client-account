﻿using System;

namespace eCall.ClientAccount.Services.DTO.TrueRS
{
    public class VoiceRecordDTO
    {
        public int vrID { get; set; }
        public string Agent { get; set; }
        public string SkillGroup { get; set; }
        public DateTime StartTime { get; set; }
        public int? Duration { get; set; }
        public int? Hold { get; set; }
        public string ANI { get; set; }
        public string DNIS { get; set; }
        public string TN { get; set; }
        public string FileName { get; set; }
        public string EndCall { get; set; }
    }
}
