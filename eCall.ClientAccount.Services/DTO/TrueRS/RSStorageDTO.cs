﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.DTO.TrueRS
{
    public class RSStorageDTO
    {
        public int stID { get; set; }
        public string Path { get; set; }
    }
}
