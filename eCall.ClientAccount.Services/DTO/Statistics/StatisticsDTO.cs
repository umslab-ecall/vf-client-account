﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.DTO.Statistics
{
    public class StatisticsDTO
    {
        public string Hours { get; set; }
        public Int32 Promo { get; set; }
        public Int32 ReceivedCalls { get; set; }
        public Double ReceivedDivPromo { get; set; }
        public Double ConfirmedPromo { get; set; }
        public Int32 ServedCallsTotal { get; set; }
        public Int32 ServedCalls5 { get; set; }
        public Int32 ServedCalls20 { get; set; }
        public Int32 ServedCalls30 { get; set; }
        public Int32 ServedCalls60 { get; set; }
        public Int32 ServedCallsUp60 { get; set; }
        public Int32 AWT { get; set; }
        public Int32 AbandonsCallsTotal { get; set; }
        public Int32 AbandonsCalls5 { get; set; }
        public Int32 AbandonsCalls20 { get; set; }
        public Int32 AbandonsCalls30 { get; set; }
        public Int32 AbandonsCalls60 { get; set; }
        public Int32 AbandonsCallsUp60 { get; set; }
        public Double LCR { get; set; }
        public Int32 RejectedCalls { get; set; }
        public Int32 ServedInTime { get; set; }
        public Double ServiceLevel { get; set; }
        public Int32 AverageProcessingTime { get; set; }
        public Int32 AverageHoldTime { get; set; }
        public Int32 OperatorGraph { get; set; }
        public Int32 OperatorLogon { get; set; }
        public Int32 SumTH { get; set; }
        public Int32 AbandonsCalls { get; set; }
    }
}
