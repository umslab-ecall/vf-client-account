﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.DTO.Statistics
{
    public class StatisticsOutStatusDTO
    {
        public string CallStatus { get; set; }
        public string CountRec { get; set; }
        public string RoundRec { get; set; }
    }

    public class StatisticsOutStatusCallDTO
    {
        public string Result { get; set; }
        public string CountRec { get; set; }
        public string RoundRec { get; set; }
    }

    public class StatisticsOutStatusNotCallDTO
    {
        public string Result { get; set; }
        public string CountRec { get; set; }
        public string RoundRec { get; set; }
    }
}
