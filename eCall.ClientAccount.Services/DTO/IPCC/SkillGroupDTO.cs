﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.DTO.IPCC
{
    public class SkillGroupDTO
    {
        public int SkillTargetID { get; set; }
        public int? ScheduleID { get; set; }
        public Int16 PeripheralID { get; set; }
        public string EnterpriseName { get; set; }
        public int PeripheralNumber { get; set; }
        public string PeripheralName { get; set; }
        public Int16 Priority { get; set; }
        public int? BaseSkillTargetID { get; set; }
    }

    public class CustomerSGDTO
    {
        public int SkillTargetID { get; set; }
    }
}
