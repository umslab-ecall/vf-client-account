﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.DTO.IPCC
{
    public class SkillGroupFilterDTO
    {
        public int SkillTargetID { get; set; }
        public string EnterpriseName { get; set; }
    }
}
