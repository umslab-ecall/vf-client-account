﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.DTO.IPCC
{
    public class CallTypeFilterDTO
    {
        public int CallTypeID { get; set; }
        public string EnterpriseName { get; set; }
    }
}
