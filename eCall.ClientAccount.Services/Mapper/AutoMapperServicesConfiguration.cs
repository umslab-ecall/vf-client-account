﻿using AutoMapper;
using eCall.ClientAccount.Services.DTO.TrueRS;
using eCall.TrueRS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Mapper
{
    public static class AutoMapperServicesConfiguration
    {
        public static void Initialize()
        {
            MapperConfiguration = new MapperConfiguration(cfg =>
            {
                // ----- Record -----
                cfg.CreateMap<Record, RecordDTO>();
                cfg.CreateMap<RecordDTO, Record>();

                // ----- RSStorage -----
                cfg.CreateMap<RSStorage, RSStorageDTO>();
                cfg.CreateMap<RSStorageDTO, RSStorage>();
            });
            Mapper = MapperConfiguration.CreateMapper();
        }
        public static IMapper Mapper { get; private set; }

        public static MapperConfiguration MapperConfiguration { get; private set; }
    }
}
