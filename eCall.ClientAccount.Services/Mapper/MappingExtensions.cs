﻿using eCall.ClientAccount.Services.DTO.TrueRS;
using eCall.TrueRS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Mapper
{
    public static class MappingExtensions
    {
        #region Mapper.Map

        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return AutoMapperServicesConfiguration.Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return AutoMapperServicesConfiguration.Mapper.Map(source, destination);
        }

        #endregion
        
        #region Record

        public static RecordDTO ToModel(this Record entity)
        {
            return entity.MapTo<Record, RecordDTO>();
        }

        public static Record ToEntity(this RecordDTO model)
        {
            return model.MapTo<RecordDTO, Record>();
        }

        public static Record ToEntity(this RecordDTO model, Record destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region RSStorage

        public static RSStorageDTO ToModel(this RSStorage entity)
        {
            return entity.MapTo<RSStorage, RSStorageDTO>();
        }

        public static RSStorage ToEntity(this RSStorageDTO model)
        {
            return model.MapTo<RSStorageDTO, RSStorage>();
        }

        public static RSStorage ToEntity(this RSStorageDTO model, RSStorage destination)
        {
            return model.MapTo(destination);
        }

        #endregion
    }
}
