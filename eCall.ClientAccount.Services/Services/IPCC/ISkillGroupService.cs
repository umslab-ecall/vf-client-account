﻿using eCall.ClientAccount.Services.DTO.IPCC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.IPCC
{
    public interface ISkillGroupService : IDisposable
    {
        IList<SkillGroupDTO> GetSGForCustomer(int userId, string type);
        void Dispose();
    }
}
