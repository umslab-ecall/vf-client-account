﻿using AutoMapper;
using eCall.ClientAccount.Core.Entities;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.DTO.IPCC;
using eCall.IPCC.Core.Entities;
using eCall.IPCC.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.IPCC
{
    public class SkillGroupService : ISkillGroupService
    {
        IUnitOfWorkCA dbca { get; set; }
        IUnitOfWorkIPCC dbipcc { get; set; }

        public SkillGroupService(IUnitOfWorkCA uow, IUnitOfWorkIPCC uowipcc)
        {
            dbca = uow;
            dbipcc = uowipcc;
        }

        public IList<SkillGroupDTO> GetSGForCustomer(int userId, string type)
        {
            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<CustomerSG, CustomerSGDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            var customersg = iMapper.Map<IEnumerable<CustomerSG>, List<CustomerSGDTO>>(dbca.CustomerAttributes.AllSGForCustomer(userId, type));

            //string text = String.Join(", ", customersg.Select(x => x.SkillTargetID));

            // применяем автомаппер для проекции одной коллекции на другую
            var config1 = new MapperConfiguration(cfg => {
                cfg.CreateMap<Skill_Group, SkillGroupDTO>();
            });
            IMapper iMapper1 = config1.CreateMapper();
            var modelDto = iMapper1.Map<IEnumerable<Skill_Group>, List<SkillGroupDTO>>(dbipcc.SkillGroups.AllManyId(String.Join(", ", customersg.Select(x => x.SkillTargetID))));
            dbca.Commit();
            dbipcc.Commit();

            return modelDto;
        }

        public void Dispose()
        {
            dbca.Dispose();
            dbipcc.Dispose();
        }
    }
}
