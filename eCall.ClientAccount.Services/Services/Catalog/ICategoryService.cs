﻿using eCall.ClientAccount.Services.DTO.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Catalog
{
    public interface ICategoryService : IDisposable
    {
        IEnumerable<CategoryDTO> GetAllCategoryForCustomer(int customerId);

        IEnumerable<ReportCategoryDTO> GetAllReportCategoryForCustomer(int customerId);
    }
}
