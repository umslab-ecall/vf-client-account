﻿using AutoMapper;
using eCall.ClientAccount.Core.Entities.Catalog;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.DTO.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Catalog
{
    public class CategoryService : ICategoryService
    {
        IUnitOfWorkCA Database { get; set; }

        public CategoryService(IUnitOfWorkCA uow)
        {
            Database = uow;
        }

        public IEnumerable<CategoryDTO> GetAllCategoryForCustomer(int customerId)
        {
            

                var config = new MapperConfiguration(cfg => {
                    cfg.CreateMap<Category, CategoryDTO>();
                });
            IMapper iMapper = config.CreateMapper();
            var cat = iMapper.Map<IEnumerable<Category>, List<CategoryDTO>>(Database.Categories.GetAll(customerId));
            Database.Commit();

            return cat;
        }

        public IEnumerable<ReportCategoryDTO> GetAllReportCategoryForCustomer(int customerId)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<ReportCategory, ReportCategoryDTO>();
            });
            IMapper iMapper = config.CreateMapper();

            var modelDto = iMapper.Map<IEnumerable<ReportCategory>, List<ReportCategoryDTO>>(Database.Categories.GetReportCategory(customerId));
            Database.Commit();

            return modelDto;
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
