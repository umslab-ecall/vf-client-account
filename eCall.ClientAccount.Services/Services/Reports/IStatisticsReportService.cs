﻿using eCall.ClientAccount.Services.DTO.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Reports
{
    public interface IStatisticsReportService : IDisposable
    {
        IList<StatisticsDTO> GetStatisticsInbSearch(string sd, string ed, string SkillGroupId, int userId, string type);
        void GetStatisticsOut(string sd, string ed, out List<StatisticsOutStatusCallDTO> liststatusCall, out List<StatisticsOutStatusDTO> liststatus, out List<StatisticsOutStatusNotCallDTO> liststatusNotCall);
        
    }
}
