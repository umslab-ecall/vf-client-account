﻿using eCall.ClientAccount.Services.DTO.IPCC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Reports
{
    public interface ICallTypeReportService : IDisposable
    {
        IList<CallTypeFilterDTO> GetCTForCustomer(int userId, string type);
        
    }
}
