﻿using AutoMapper;
using eCall.ClientAccount.Core.Entities.IPCC;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.DTO.IPCC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Reports
{
    public class SkillGroupReportService : ISkillGroupReportService
    {
        IUnitOfWorkCA dbca { get; set; }

        public SkillGroupReportService(IUnitOfWorkCA uowca)
        {
            dbca = uowca;
        }

        public IList<SkillGroupFilterDTO> GetSGRecordForCustomer(int userId)
        {
            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<SkillGroupFilter, SkillGroupFilterDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            var modelDto = iMapper.Map<IEnumerable<SkillGroupFilter>, List<SkillGroupFilterDTO>>(dbca.SkillGroup.GetAllCustomerSGFilter(userId));
            dbca.Commit();

            return modelDto;
        }

        public IList<SkillGroupFilterDTO> GetSGRecordForCustomer(int userId, string type)
        {
            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<SkillGroupFilter, SkillGroupFilterDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            var modelDto = iMapper.Map<IEnumerable<SkillGroupFilter>, List<SkillGroupFilterDTO>>(dbca.SkillGroup.GetAllCustomerSGFilter(userId, type));
            dbca.Commit();

            return modelDto;
        }

        public void Dispose()
        {
            dbca.Dispose();
        }
    }
}
