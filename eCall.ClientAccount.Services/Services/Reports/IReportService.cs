﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Reports
{
    public interface IReportService : IDisposable
    {
        List<Dictionary<string, object>> ExecuteReport(int repid, string skillgroupid, string StartDate, string EndDate, int customerId);
    }
}
