﻿using AutoMapper;
using eCall.ClientAccount.Core.Entities.Reports;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.DTO.Statistics;
using eCall.ClientAccount.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Reports
{
    public class StatisticsReportService : IStatisticsReportService
    {
        IUnitOfWorkCA dbca { get; set; }

        public StatisticsReportService(IUnitOfWorkCA uowca)
        {
            dbca = uowca;
        }


        public IList<StatisticsDTO> GetStatisticsInbSearch(string sd, string ed, string SkillGroupId, int userId, string type)
        {
            DateTime dd = DateTime.Now;
            DateTime sDate = DateTime.Now, eDate = DateTime.Now;

            if (String.IsNullOrEmpty(sd))
            {
                sDate = new DateTime(dd.Year, dd.Month, dd.Day, 0, 0, 0);
                eDate = new DateTime(dd.Year, dd.Month, dd.Day, 23, 59, 59);
                sd = sDate.ToString("yyyy-MM-dd HH:mm:ss");
                ed = eDate.ToString("yyyy-MM-dd HH:mm:ss");
            } else
            {
                sd = DateTimeHelper.DateToStringSQLFormat(sd);
                ed = DateTimeHelper.DateToStringSQLFormat(ed);
            }
            //else
            //{
            //    sd = sd + " 00:00:00";
            //    ed = ed + " 23:59:59";
            //}

            if(SkillGroupId == "-1" || SkillGroupId == "")
            {
                var sg = dbca.SkillGroup.GetAllCustomerSGFilter(userId, type);
                string SGtoCT = String.Join(", ", sg.Select(x => x.SkillTargetID));
                SkillGroupId = dbca.Report.SkillGroupToCallType(SGtoCT);
            }
            else
            {
                SkillGroupId = dbca.Report.SkillGroupToCallType(SkillGroupId);
            }

            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<StatisticsSearch, StatisticsDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            var modelDto = iMapper.Map<IEnumerable<StatisticsSearch>, List<StatisticsDTO>>(dbca.Report.StatisticsInb(sd, ed, SkillGroupId));
            dbca.Commit();

            return modelDto;
        }

        public void GetStatisticsOut(string sd, string ed, out List<StatisticsOutStatusCallDTO> liststatusCall, out List<StatisticsOutStatusDTO> liststatus, out List<StatisticsOutStatusNotCallDTO> liststatusNotCall)
        {
            sd = DateTimeHelper.DateToStringSQLFormat(sd); // + " 00:00:00";
            ed = DateTimeHelper.DateToStringSQLFormat(ed); // + " 23:59:59";


            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<StatisticsOutStatus, StatisticsOutStatusDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            liststatus = iMapper.Map<IEnumerable<StatisticsOutStatus>, List<StatisticsOutStatusDTO>>(dbca.Report.StatisticsOutStatus(sd, ed));

            // применяем автомаппер для проекции одной коллекции на другую
            var config1 = new MapperConfiguration(cfg => {
                cfg.CreateMap<StatisticsOutStatusCall, StatisticsOutStatusCallDTO>();
            });
            IMapper iMapper1 = config1.CreateMapper();
            liststatusCall = iMapper1.Map<IEnumerable<StatisticsOutStatusCall>, List<StatisticsOutStatusCallDTO>>(dbca.Report.StatisticsOutStatusCall(sd, ed));
            

            // применяем автомаппер для проекции одной коллекции на другую
            var config2 = new MapperConfiguration(cfg => {
                cfg.CreateMap<StatisticsOutStatusNotCall, StatisticsOutStatusNotCallDTO>();
            });
            IMapper iMapper2 = config2.CreateMapper();
            liststatusNotCall = iMapper2.Map<IEnumerable<StatisticsOutStatusNotCall>, List<StatisticsOutStatusNotCallDTO>>(dbca.Report.StatisticsOutStatusNotCall(sd, ed));
            dbca.Commit();
        }

        public void Dispose()
        {
            dbca.Dispose();
        }
    }
}
