﻿using AutoMapper;
using eCall.ClientAccount.Core.Entities.IPCC;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.DTO.IPCC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Reports
{
    public class CallTypeReportService : ICallTypeReportService
    {
        IUnitOfWorkCA dbca { get; set; }

        public CallTypeReportService(IUnitOfWorkCA uowca)
        {
            dbca = uowca;
        }

        public IList<CallTypeFilterDTO> GetCTForCustomer(int userId, string type)
        {
            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<CallTypeFilter, CallTypeFilterDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            var modelDto = iMapper.Map<IEnumerable<CallTypeFilter>, List<CallTypeFilterDTO>>(dbca.CallType.GetAllCustomerCTFilterType(userId, type));
            dbca.Commit();

            return modelDto;
        }

        public void Dispose()
        {
            dbca.Dispose();
        }
    }
}
