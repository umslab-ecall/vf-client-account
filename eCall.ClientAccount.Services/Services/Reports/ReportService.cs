﻿using eCall.ClientAccount.Core.Entities.Reports;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Reports
{
    public class ReportService : IReportService
    {
        IUnitOfWorkCA dbca { get; set; }

        public ReportService(IUnitOfWorkCA uowca)
        {
            dbca = uowca;
        }

        public List<Dictionary<string, object>> ExecuteReport(int repid, string skillgroupid, string StartDate, string EndDate, int customerId)
        {
            StartDate = DateTimeHelper.DateToStringSQLFormat(StartDate); // + " 00:00:00";
            EndDate = DateTimeHelper.DateToStringSQLFormat(EndDate); // + " 23:59:59";

            Report reports = dbca.Report.GetReportById(repid);

            if(!reports.Active)
            {
                var result = new List<Dictionary<string, object>>();
                return result;
            }

            if(skillgroupid == "-1")
            {
                var sg = dbca.SkillGroup.GetAllCustomerSGFilter(customerId);
                skillgroupid = String.Join(", ", sg.Select(x => x.SkillTargetID));
            }

            if(reports.Param == "ctid")
            {
                skillgroupid = dbca.Report.SkillGroupToCallType(skillgroupid);
            }

            //string projectname = dbca.Report.GetCompaingNameByCustomer(customerId);

            var rep = dbca.Report.ExecutSP(reports.StoredProc, skillgroupid, StartDate, EndDate);
            dbca.Commit();

            return rep;
        }

        public void Dispose()
        {
            dbca.Dispose();            
        }
    }
}
