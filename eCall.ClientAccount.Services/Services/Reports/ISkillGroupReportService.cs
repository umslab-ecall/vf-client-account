﻿using eCall.ClientAccount.Services.DTO.IPCC;
using System;
using System.Collections.Generic;

namespace eCall.ClientAccount.Services.Services.Reports
{
    public interface ISkillGroupReportService : IDisposable
    {
        IList<SkillGroupFilterDTO> GetSGRecordForCustomer(int userId);
        IList<SkillGroupFilterDTO> GetSGRecordForCustomer(int userId, string type);
        
    }
}
