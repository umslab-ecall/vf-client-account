﻿using AutoMapper;
using eCall.ClientAccount.Services.DTO.Security;
using eCall.Identity.Core.Entities.Domains;
using eCall.Identity.Core.Interfaces;
using System.Collections.Generic;

namespace eCall.ClientAccount.Services.Services.Security
{
    public class DomainService : IDomainService
    {
        IUnitOfWork Database { get; set; }

        public DomainService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public IEnumerable<DomainDTO> GetAllDomains()
        {
            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<ADomain, DomainDTO>();
            });
            IMapper iMapper = config.CreateMapper();

            return iMapper.Map<IEnumerable<ADomain>, List<DomainDTO>>(Database.Domains.GetAllDomainsRep());
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
