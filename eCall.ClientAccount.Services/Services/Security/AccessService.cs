﻿using eCall.ClientAccount.Core.Interfaces;

namespace eCall.ClientAccount.Services.Services.Security
{
    public class AccessService : IAccessService
    {
        IUnitOfWorkCA Database { get; set; }

        public AccessService(IUnitOfWorkCA uow)
        {
            Database = uow;
        }

        public bool AccessCategory(int user, string url)
        {
            int accesscat = Database.Access.AccessCategoryUser(user, url);
            Database.Commit();
            if (accesscat > 0)
                return false;
            else
                return true;
        }

        public bool AccessReport(int user, int reportId)
        {
            int accesscat = Database.Access.AccessReportUser(user, reportId);
            Database.Commit();
            if (accesscat > 0)
                return false;
            else
                return true;
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
