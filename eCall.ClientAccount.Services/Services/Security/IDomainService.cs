﻿using eCall.ClientAccount.Services.DTO.Security;
using System;
using System.Collections.Generic;

namespace eCall.ClientAccount.Services.Services.Security
{
    public interface IDomainService : IDisposable
    {
        IEnumerable<DomainDTO> GetAllDomains();
        
    }
}
