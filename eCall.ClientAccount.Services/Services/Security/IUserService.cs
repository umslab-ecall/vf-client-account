﻿using eCall.ClientAccount.Services.DTO.Security;
using System;

namespace eCall.ClientAccount.Services.Services.Security
{
    public interface IUserService : IDisposable
    {
        UserDTO GetUserByUserName(string userName);
    }
}
