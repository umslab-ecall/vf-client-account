﻿using eCall.ClientAccount.Services.Infrastructure;
using eCall.Identity.Core.Entities.Customers;
using System.DirectoryServices.AccountManagement;

namespace eCall.ClientAccount.Services.Services.Security
{
    public class ActiveDirectoryService : IActiveDirectoryService
    {
        public bool ValidateCredentials(string domain, string userName, string password)
        {
            userName = userName.EnsureNotNull();
            userName = userName.Trim();

            password = password.EnsureNotNull();
            password = password.Trim();

            using (var context = new PrincipalContext(ContextType.Domain, domain))
            {
                return context.ValidateCredentials(userName, password);
            }
        }

        public Customer GetUserFromAd(string domain, string userName)
        {
            Customer result = null;
            userName = userName.EnsureNotNull();
            userName = userName.Trim();

            using (var context = new PrincipalContext(ContextType.Domain, domain))
            {
                var user = UserPrincipal.FindByIdentity(context, userName);
                if (user != null)
                {
                    result = new Customer
                    {
                        Id = 0,
                        UserName = userName,
                        FirstName = user.GivenName,
                        LastName = user.Surname,
                        Active = true
                    };
                }
            }
            return result;
        }
    }
}
