﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Security
{
    public interface IAccessService : IDisposable
    {
        bool AccessCategory(int user, string url);
        bool AccessReport(int user, int reportId);

        
    }
}
