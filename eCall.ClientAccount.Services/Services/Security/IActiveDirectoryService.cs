﻿using eCall.Identity.Core.Entities.Customers;
using System;

namespace eCall.ClientAccount.Services.Services.Security
{
    public interface IActiveDirectoryService
    {
        bool ValidateCredentials(string domain, string userName, string password);
        Customer GetUserFromAd(string domain, string userName);
    }
}
