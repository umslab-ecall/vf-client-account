﻿using eCall.ClientAccount.Services.DTO.Security;
using eCall.Identity.Core.Entities.Customers;
using eCall.Identity.Core.Interfaces;
using System.Collections.Generic;

namespace eCall.ClientAccount.Services.Services.Security
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }

        public UserService(IUnitOfWork uow)
        {
            Database = uow;
        }
        
        public UserDTO GetUserByUserName(string userName)
        {
            var user = Database.Users.GetUserByUserNameAsync(userName);

            IList<RoleDTO> roleDto = new List<RoleDTO>();

            var roles = Database.Users.GetRolesAsync(user);


            foreach (Role role in roles)
            {
                RoleDTO roleD = new RoleDTO();
                roleD.Id = role.Id;
                roleD.Name = role.Name;

                roleDto.Add(roleD);
            }

            UserDTO userDto = new UserDTO
            {
                Id = user.Id,
                Active = user.Active,
                CreatedBy = user.CreatedBy,
                UserName = user.UserName,
                CreatedOn = user.CreatedOn,
                FirstName = user.FirstName,
                LastName = user.LastName,
                LastLoginDate = user.LastLoginDate,
                ModifiedBy = user.ModifiedBy,
                ModifiedOn = user.ModifiedOn,
                Roles = roleDto
            };


            return userDto;
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
