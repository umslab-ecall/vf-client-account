﻿using eCall.ClientAccount.Services.DTO.Security;
using System;
using System.Collections.Generic;

namespace eCall.ClientAccount.Services.Services.Security
{
    public interface IAuthenticationService
    {
        void SignIn(UserDTO user, IList<string> roleNames);
        void SignOut();
    }
}
