﻿using eCall.ClientAccount.Services.DTO.TrueRS;
using System.Collections.Generic;

namespace eCall.ClientAccount.Services.Services.TrueRS
{
    public interface ISkillGroupRecordService
    {
        IList<SkillGroupRecordDTO> GetSGRecordForCustomer(int userId, string type);
        void Dispose();
    }
}
