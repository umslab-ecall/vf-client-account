﻿using AutoMapper;
using eCall.ClientAccount.Core.Entities.TrueRS;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.DTO.TrueRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.TrueRS
{
    public class SkillGroupRecordService : ISkillGroupRecordService
    {
        IUnitOfWorkCA dbca { get; set; }

        public SkillGroupRecordService(IUnitOfWorkCA uow)
        {
            dbca = uow;
        }

        public IList<SkillGroupRecordDTO> GetSGRecordForCustomer(int userId, string type)
        {
            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<SkillGroupRecord, SkillGroupRecordDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            var modelDto = iMapper.Map<IEnumerable<SkillGroupRecord>, List<SkillGroupRecordDTO>>(dbca.RecordAttrs.AllSGRecordForCustomer(userId, type));
            dbca.Commit();

            return modelDto;
        }

        public void Dispose()
        {
            dbca.Dispose();
        }
    }
}
