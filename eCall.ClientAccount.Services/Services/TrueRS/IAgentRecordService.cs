﻿using eCall.ClientAccount.Services.DTO.TrueRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.TrueRS
{
    public interface IAgentRecordService
    {
        IList<AgentRecordDTO> GetAgentBySG(int userId, string type);
        void Dispose();
    }
}
