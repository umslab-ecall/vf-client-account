﻿using AutoMapper;
using eCall.ClientAccount.Core.Entities.TrueRS;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.DTO.TrueRS;
using eCall.ClientAccount.Services.Infrastructure;
using eCall.ClientAccount.Services.Mapper;
using eCall.TrueRS.Core.Entities;
using eCall.TrueRS.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.TrueRS
{
    public class VoiceRecordService : IVoiceRecordService
    {
        #region Fields

        IUnitOfWorkTrueRS dbtruers { get; set; }
        IUnitOfWorkCA dbca { get; set; }

        #endregion

        #region Ctor

        public VoiceRecordService(IUnitOfWorkTrueRS uowtruers, IUnitOfWorkCA uowca)
        {
            dbtruers = uowtruers;
            dbca = uowca;
        }

        #endregion

        #region Methods

        public IList<VoiceRecordDTO> GetRecordByCustomer(DateTime startd, DateTime endd, string sgid, string phone, string agentid, int? top, int userId, string type, int? MinDuration, int? MaxDuration)
        {
            string startDate, endDate;
            IList<int> sglist = new List<int>();// = new int[] { };
            IList<int> agentList = new List<int>();// = new int[] { };
            int[] sglist1 = new int[100];
            int top1 = 100;
            if (top != null)
                top1 = Convert.ToInt32(top);
            if (startd == Convert.ToDateTime("01.01.0001 00:00:00") || endd == Convert.ToDateTime("01.01.0001 00:00:00"))
            {
                DateTime datenow = DateTime.Now;
                startd = new DateTime(datenow.Year, datenow.Month, datenow.Day, 0, 0, 0);
                endd = new DateTime(datenow.Year, datenow.Month, datenow.Day, 23, 59, 59);
            }

            if (startd > endd)
            {
                startDate = endd.ToString("yyyy-MM-dd HH:mm:ss");
                endDate = startd.ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                endDate = endd.ToString("yyyy-MM-dd HH:mm:ss");
                startDate = startd.ToString("yyyy-MM-dd HH:mm:ss");
            }

            if (sgid == "-1")
            {
                var sg = dbca.RecordAttrs.AllSGRecordForCustomer(userId, type);
                sgid = String.Join(", ", sg.Select(x => x.SkillsetID));
            }
            if (agentid == "-1")
            {
                var agent = dbca.RecordAttrs.AllAgentBySG(sgid);
                agentid = String.Join(", ", agent.Select(x => x.SkillTargetID));
            }

            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<VoiceRecord, VoiceRecordDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            if (String.IsNullOrEmpty(phone))
            {
                var modelDto = iMapper.Map<IList<VoiceRecord>, IList<VoiceRecordDTO>>(dbtruers.VoiceRecords.AllRecordByCustomer(startDate, endDate, sgid, agentid, top1, MinDuration, MaxDuration));
                dbtruers.Commit();
                dbca.Commit();

                if (type == "Inbound")
                {
                    foreach (var record in modelDto)
                    {
                        EndCallResult endCall = dbca.RecordAttrs.GetEndCallResult(record.ANI, record.StartTime.ToString("yyyy-MM-dd HH:mm:ss"));
                        if (endCall.OrigCauseValue == 0 && endCall.DestCauseValue == 16 && endCall.is_transfer == 0)
                            record.EndCall = "Оператор";
                        if (endCall.OrigCauseValue == 16 && endCall.DestCauseValue == 0 && endCall.is_transfer == 0)
                            record.EndCall = "Абонент";
                        if ((endCall.OrigCauseValue != 16 && endCall.OrigCauseValue != 0) && (endCall.DestCauseValue != 16 && endCall.DestCauseValue != 0) && endCall.is_transfer == 0)
                            record.EndCall = "Ошибка";
                        if (endCall.is_transfer == 1)
                            record.EndCall = "Трансфер";
                    }
                }


                return modelDto;
            }
            else
            {
                var modelDto = iMapper.Map<IList<VoiceRecord>, IList<VoiceRecordDTO>>(dbtruers.VoiceRecords.AllRecordByCustomer(startDate, endDate, sgid, "%" + phone + "%", agentid, top1, MinDuration, MaxDuration));
                dbtruers.Commit();
                dbca.Commit();
                if (type == "Inbound")
                {
                    foreach (var record in modelDto)
                    {
                        EndCallResult endCall = dbca.RecordAttrs.GetEndCallResult(record.ANI, record.StartTime.ToString("yyyy-MM-dd HH:mm:ss"));
                        if (endCall.OrigCauseValue == 0 && endCall.DestCauseValue == 16 && endCall.is_transfer == 0)
                            record.EndCall = "Оператор";
                        if (endCall.OrigCauseValue == 16 && endCall.DestCauseValue == 0 && endCall.is_transfer == 0)
                            record.EndCall = "Абонент";
                        if ((endCall.OrigCauseValue != 16 && endCall.OrigCauseValue != 0) && (endCall.DestCauseValue != 16 && endCall.DestCauseValue != 0) && endCall.is_transfer == 0)
                            record.EndCall = "Ошибка";
                        if (endCall.is_transfer == 1)
                            record.EndCall = "Трансфер";
                    }
                }


                return modelDto;
            }
        }

        public IList<RecordDTO> GetRecordById(int id, string serverpath, string browser)
        {
            var record = dbtruers.VoiceRecords.GetRecordById(id).Select(x => x.ToModel()).ToList();

            string firstAni = record.Select(x => x.ANI).First();
            DateTime firstStartTime = record.Select(x => x.StartTime).First();

            EndCallResult endCall = dbca.RecordAttrs.GetEndCallResult(firstAni, firstStartTime.ToString("yyyy-MM-dd HH:mm:ss"));

            if (endCall.OrigCauseValue == 0 && endCall.DestCauseValue == 16 && endCall.is_transfer == 0)
                record.First().EndCall = "Оператор";
            if (endCall.OrigCauseValue == 16 && endCall.DestCauseValue == 0 && endCall.is_transfer == 0)
                record.First().EndCall = "Абонент";
            if ((endCall.OrigCauseValue != 16 && endCall.OrigCauseValue != 0) && (endCall.DestCauseValue != 16 && endCall.DestCauseValue != 0) && endCall.is_transfer == 0)
                record.First().EndCall = "Ошибка";
            if (endCall.is_transfer == 1)
                record.First().EndCall = "Трансфер";

            var test = PrepareRecord.PrepareRecordForPlay(record, id, serverpath, browser);
            dbtruers.Commit();
            dbca.Commit();

            return record;
        }

        public MP3RecordDTO GetMP3RecordById(int id, string serverpath, string browser)
        {
            var record = dbtruers.VoiceRecords.GetRecordById(id).Select(x => x.ToModel()).ToList();

            string firstAni = record.Select(x => x.ANI).First();
            DateTime firstStartTime = record.Select(x => x.StartTime).First();

            EndCallResult endCall = dbca.RecordAttrs.GetEndCallResult(firstAni, firstStartTime.ToString("yyyy-MM-dd HH:mm:ss"));

            if (endCall != null)
            {
                if (endCall.OrigCauseValue == 0 && endCall.DestCauseValue == 16 && endCall.is_transfer == 0)
                    record.First().EndCall = "Оператор";
                if (endCall.OrigCauseValue == 16 && endCall.DestCauseValue == 0 && endCall.is_transfer == 0)
                    record.First().EndCall = "Абонент";
                if ((endCall.OrigCauseValue != 16 && endCall.OrigCauseValue != 0) && (endCall.DestCauseValue != 16 && endCall.DestCauseValue != 0) && endCall.is_transfer == 0)
                    record.First().EndCall = "Ошибка";
                if (endCall.is_transfer == 1)
                    record.First().EndCall = "Трансфер";
            }

            var resultRecord = PrepareMP3Record.PrepareMP3RecordForPlay(record, id, serverpath, browser);

            if (resultRecord == null)
            {
                List<RSStorageDTO> storege = dbtruers.RSStorages.GetStorageAll().Select(x => x.ToModel()).ToList();
                string fileName = dbtruers.VoiceRecords.GetRecordFileNameById(id);

                if (!String.IsNullOrEmpty(fileName))
                {
                    foreach (var stor in storege)
                    {
                        var impersonationContext = new WrappedImpersonationContext("IPCC", "TrueRS_Tech", "lGAOsRsgDhfboqSI0xnr");
                        impersonationContext.Enter();

                        if (System.IO.File.Exists(stor.Path.ToString() + @"\" + fileName))
                        {
                            dbtruers.VoiceRecords.UpdateRecordstIDById(id, stor.stID);
                            dbtruers.Commit();
                        }

                        impersonationContext.Leave();
                    }
                }

                record = dbtruers.VoiceRecords.GetRecordById(id).Select(x => x.ToModel()).ToList();
                resultRecord = PrepareMP3Record.PrepareMP3RecordForPlay(record, id, serverpath, browser);
            }

            dbtruers.Commit();
            dbca.Commit();

            MP3RecordDTO recDto = new MP3RecordDTO
            {
                vrID = record.First().vrID,
                Agent = record.First().Agent,
                SkillGroup = record.First().SkillGroup,
                StartTime = record.First().StartTime,
                Duration = record.First().Duration,
                Hold = resultRecord.First().Hold,
                ANI = record.First().ANI,
                DNIS = record.First().DNIS,
                TN = record.First().TN,
                EndCall = record.First().EndCall,
                FilePath = resultRecord.First().FilePath
            };

            return recDto;
        }

        #endregion
        
        #region Dispose

        public void Dispose()
        {
            dbca.Dispose();
            dbtruers.Dispose();
        }

        #endregion
    }
}
