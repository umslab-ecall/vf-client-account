﻿using eCall.ClientAccount.Services.DTO.TrueRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.TrueRS
{
    public interface IVoiceRecordService
    {
        IList<VoiceRecordDTO> GetRecordByCustomer(DateTime startd, DateTime endd, string sgid, string phone, string agentid, int? top, int userId, string type, int? MinDuration, int? MaxDuration);

        IList<RecordDTO> GetRecordById(int id, string serverpath, string browser);

        MP3RecordDTO GetMP3RecordById(int id, string serverpath, string browser);
        void Dispose();
    }
}
