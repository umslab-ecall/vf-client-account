﻿using AutoMapper;
using eCall.ClientAccount.Core.Entities.TrueRS;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.DTO.TrueRS;
using eCall.TrueRS.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eCall.ClientAccount.Services.Services.TrueRS
{
    public class AgentRecordService : IAgentRecordService
    {
        IUnitOfWorkCA dbca { get; set; }
        IUnitOfWorkTrueRS dbtruers { get; set; }

        public AgentRecordService(IUnitOfWorkCA uow, IUnitOfWorkTrueRS uowtruers)
        {
            dbca = uow;
            dbtruers = uowtruers;
        }
        
        public IList<AgentRecordDTO> GetAgentBySG(int userId, string type)
        {
            // применяем автомаппер для проекции одной коллекции на другую
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<SkillGroupRecord, SkillGroupRecordDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            var customersg = iMapper.Map<IEnumerable<SkillGroupRecord>, List<SkillGroupRecordDTO>>(dbca.RecordAttrs.AllSGRecordForCustomer(userId, type));

            //string text = String.Join(", ", customersg.Select(x => x.SkillTargetID));
            if(customersg.Count == 0)
            {
                return new List<AgentRecordDTO>();
            }

            // применяем автомаппер для проекции одной коллекции на другую
            var config1 = new MapperConfiguration(cfg => {
                cfg.CreateMap<AgentRecord, AgentRecordDTO>();
            });
            IMapper iMapper1 = config1.CreateMapper();
            var modelDto = iMapper1.Map<IEnumerable<AgentRecord>, List<AgentRecordDTO>>(dbca.RecordAttrs.AllAgentBySG(String.Join(", ", customersg.Select(x => x.SkillsetID))));
            dbca.Commit();
            dbtruers.Commit();

            return modelDto;
        }

        public void Dispose()
        {
            dbca.Dispose();
            dbtruers.Dispose();
        }
    }
}
