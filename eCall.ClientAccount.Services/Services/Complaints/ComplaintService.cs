﻿using AutoMapper;
using eCall.ClientAccount.Core.Entities.Complaints;
using eCall.ClientAccount.Core.Interfaces;
using eCall.ClientAccount.Services.DTO.Complaints;
using eCall.ClientAccount.Services.Helpers;
using eCall.Identity.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Complaints
{
    public class ComplaintService : IComplaintService
    {
        IUnitOfWorkCA Database { get; set; }
        IUnitOfWork dbIdentity { get; set; }

        public ComplaintService(IUnitOfWorkCA uow, IUnitOfWork uowIdentity)
        {
            Database = uow;
            dbIdentity = uowIdentity;
        }

        public IEnumerable<ComplaintDTO> GetAllComplaint(int customerId, string sd, string ed, string phone, string rescomplaint)
        {
            sd = DateTimeHelper.DateToStringSQLFormat(sd); // + " 00:00:00";
            ed = DateTimeHelper.DateToStringSQLFormat(ed); // + " 23:59:59";

            int campaignId = Database.Campaign.GetCampaignByCustomerId(customerId);

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Complaint, ComplaintDTO>();
            });
            IMapper iMapper = config.CreateMapper();
            var modelDto = Database.Complaint.GetAll(campaignId, sd, ed, phone, rescomplaint);
            Database.Commit();
            dbIdentity.Commit();

            return iMapper.Map<IEnumerable<Complaint>, List<ComplaintDTO>>(modelDto);
        }

        public ComplaintDTO GetComplaintById(int complaintId, int customerId)
        {
            int campaignId = Database.Campaign.GetCampaignByCustomerId(customerId);

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Complaint, ComplaintDTO>();
            });
            IMapper iMapper = config.CreateMapper();

            var modelDto = Database.Complaint.GetById(complaintId, campaignId);
            Database.Commit();
            dbIdentity.Commit();

            return iMapper.Map<Complaint, ComplaintDTO>(modelDto);
        }

        public void CreateComplaint(ComplaintDTO item, int customerId)
        {
            int campaignId = Database.Campaign.GetCampaignByCustomerId(customerId);
            string customerName = dbIdentity.Users.GetUserNameById(customerId);

            Complaint complaint = new Complaint
            {
                CampaignID = campaignId,
                ComplaintDescription = item.ComplaintDescription,
                CreatedBy = customerName,
                CreatedOn = DateTime.Now,
                ComplaintTheme = item.ComplaintTheme,
                ComplaintDateTime = item.ComplaintDateTime,
                PhoneCustomer = item.PhoneCustomer,
                TypeCustomer = item.TypeCustomer,
                ComplaintComment = item.ComplaintComment,
                ComplaintInformation = item.ComplaintInformation
            };

            Database.Complaint.Add(complaint);
            Database.Commit();
            dbIdentity.Commit();
        }

        public void UpdateComplaint(ComplaintDTO item, int customerId)
        {
            int campaignId = Database.Campaign.GetCampaignByCustomerId(customerId);
            string customerName = dbIdentity.Users.GetUserNameById(customerId);

            Complaint complaint = new Complaint
            {
                CampaignID = campaignId,
                ComplaintId = item.ComplaintId,
                UpdatedBy = customerName,
                UpdatedOn = DateTime.Now,
                Comment = item.Comment,
                Responsible = item.Responsible,
                ComplaintResult = item.ComplaintResult,
                Operator = item.Operator,
                RecordCallId = item.RecordCallId
            };
            
            Database.Complaint.Update(complaint);
            Database.Commit();
            dbIdentity.Commit();
        }

        public void Dispose()
        {
            Database.Dispose();
            dbIdentity.Dispose();
        }
    }
}
