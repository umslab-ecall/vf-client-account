﻿using eCall.ClientAccount.Services.DTO.Complaints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.Complaints
{
    public interface IComplaintService : IDisposable
    {
        IEnumerable<ComplaintDTO> GetAllComplaint(int customerId, string sd, string ed, string phone, string rescomplaint);
        ComplaintDTO GetComplaintById(int complaintId, int customerId);
        void CreateComplaint(ComplaintDTO item, int customerId);
        void UpdateComplaint(ComplaintDTO item, int customerId);
    }
}
