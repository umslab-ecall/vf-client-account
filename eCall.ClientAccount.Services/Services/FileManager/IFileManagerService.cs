﻿using eCall.ClientAccount.Services.Infrastructure.FileSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.ClientAccount.Services.Services.FileManager
{
    public interface IFileManagerService : IDisposable
    {
        IEnumerable<string> Find(int take = 10, int skip = 0, string path = "", string order = "");
    }
}
