﻿using System;

namespace eCall.IPCC.Core.Entities
{
    public class Skill_Group
    {
        public int SkillTargetID { get; set; }
        public int? ScheduleID { get; set; }
        public Int16 PeripheralID { get; set; }
        public string EnterpriseName { get; set; }
        public int PeripheralNumber { get; set; }
        public string PeripheralName { get; set; }
        public Int16 Priority { get; set; }
        public int? BaseSkillTargetID { get; set; }        
    }
}
