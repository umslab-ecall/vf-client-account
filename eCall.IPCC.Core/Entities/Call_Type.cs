﻿namespace eCall.IPCC.Core.Entities
{
    public class Call_Type
    {
        public int CallTypeID { get; set; }
        public int? CustomerDefinitionID { get; set; }
        public string EnterpriseName { get; set; }
        public string Description { get; set; }
        
    }
}
