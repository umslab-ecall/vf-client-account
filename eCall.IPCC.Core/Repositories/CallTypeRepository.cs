﻿using Dapper;
using eCall.IPCC.Core.Entities;
using eCall.IPCC.Core.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace eCall.IPCC.Core.Repositories
{
    internal class CallTypeRepository : RepositoryBase, ICallType
    {
        public CallTypeRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }
        
        public IList<Call_Type> All()
        {
            var sql = @"
                        SELECT  [CallTypeID]
                                ,[CustomerDefinitionID]
                                ,[EnterpriseName]
                                ,[Description]
                        FROM [dbo].[Call_Type]
                        WHERE [EnterpriseName] like '%CC.CT%'";


            return Connection.Query<Call_Type>(sql,                
                transaction: Transaction
               ).ToList();
        }

        public Call_Type GetId(int id)
        {
            var sql = @"
                SELECT  [CallTypeID]
                        ,[CustomerDefinitionID]
                        ,[EnterpriseName]
                        ,[Description]
                FROM [dbo].[Call_Type]
                WHERE [CallTypeID] = @CallTypeID";

            return Connection.Query<Call_Type>(sql,
                param: new { CallTypeID = id },
                transaction: Transaction
            ).FirstOrDefault();
        }
    }
}
