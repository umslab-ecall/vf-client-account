﻿using Dapper;
using eCall.IPCC.Core.Entities;
using eCall.IPCC.Core.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace eCall.IPCC.Core.Repositories
{
    internal class SkillGroupRepository : RepositoryBase, ISkillGroup
    {
        public SkillGroupRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IList<Skill_Group> All()
        {
            var sql = @"
                      SELECT [SkillTargetID]
                             ,[ScheduleID]
                             ,[PeripheralID]
                            ,[EnterpriseName]
                            ,[PeripheralNumber]
                            ,[PeripheralName]
                            ,[Priority]
                            ,[BaseSkillTargetID]
                    FROM [dbo].[Skill_Group]
                    WHERE [EnterpriseName] like '%CC.SG%'";


            return Connection.Query<Skill_Group>(sql,
                transaction: Transaction
               ).ToList();
        }

        public Skill_Group GetId(int id)
        {
            var sql = @"SELECT  [SkillTargetID]
                                ,[ScheduleID]
                                ,[PeripheralID]
                                ,[EnterpriseName]
                                ,[PeripheralNumber]
                                ,[PeripheralName]
                                ,[Priority]
                                ,[BaseSkillTargetID]
                        FROM [dbo].[Skill_Group]
                        WHERE [SkillTargetID] = @SkillTargetID";

            return Connection.Query<Skill_Group>(sql,
                param: new { SkillTargetID = id },
                transaction: Transaction
            ).FirstOrDefault();
        }

        public IList<Skill_Group> AllManyId(string manysgid)
        {
            var sql = @"
                      SELECT [SkillTargetID]
                             ,[ScheduleID]
                             ,[PeripheralID]
                            ,[EnterpriseName]
                            ,[PeripheralNumber]
                            ,[PeripheralName]
                            ,[Priority]
                            ,[BaseSkillTargetID]
                    FROM [dbo].[Skill_Group]
                    WHERE [SkillTargetID] in ( @SkillTargetID )";


            return Connection.Query<Skill_Group>(sql,
                param: new { SkillTargetID = manysgid },
                transaction: Transaction
               ).ToList();
        }
    }
}
