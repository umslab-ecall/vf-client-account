﻿using eCall.IPCC.Core.Entities;
using System.Collections.Generic;

namespace eCall.IPCC.Core.Interfaces
{
    public interface ICallType
    {
        IList<Call_Type> All();
        Call_Type GetId(int id);
    }
}
