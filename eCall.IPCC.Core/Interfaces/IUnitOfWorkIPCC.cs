﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.IPCC.Core.Interfaces
{
    public interface IUnitOfWorkIPCC : IDisposable
    {
        ISkillGroup SkillGroups { get; }
        void Commit();
    }
}
