﻿using eCall.IPCC.Core.Entities;
using System.Collections.Generic;

namespace eCall.IPCC.Core.Interfaces
{
    public interface ISkillGroup
    {
        IList<Skill_Group> All();
        IList<Skill_Group> AllManyId(string manysgid);
        Skill_Group GetId(int id);
    }
}
