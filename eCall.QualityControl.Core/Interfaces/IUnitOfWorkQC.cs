﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCall.QualityControl.Core.Interfaces
{
    public interface IUnitOfWorkQC : IDisposable
    {
        void Commit();
    }
}
