﻿using eCall.QualityControl.Core.Interfaces;
using System;
using System.Data;
using System.Data.SqlClient;

namespace eCall.QualityControl.Core.Repositories
{
    public class UnitOfWorkQC : IUnitOfWorkQC
    {
        #region Fields

        private IDbConnection _connection;
        private IDbTransaction _transaction;
        //private ISkillGroup _skillGroupRepository;

        private bool _disposed;

        #endregion

        #region Constructor

        public UnitOfWorkQC(string connectionString)
        {
                _connection = new SqlConnection(connectionString);
                _connection.Open();
                _transaction = _connection.BeginTransaction();                            
        }

        #endregion

        #region Methods

        //public ISkillGroup SkillGroups
        //{
        //    get { return _skillGroupRepository ?? (_skillGroupRepository = new SkillGroupRepository(_transaction)); }
        //}

        #endregion

        #region Commit

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                resetRepositories();
                SqlConnection.ClearAllPools();
            }
        }

        #endregion

        #region ResetRepositories

        private void resetRepositories()
        {
            //_skillGroupRepository = null;
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Close();
                        _connection.Dispose();                        
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitOfWorkQC()
        {
            dispose(false);
        }

        #endregion
    }
}
